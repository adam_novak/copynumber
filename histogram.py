#!/usr/bin/env python2.7
"""
histogram: plot a histogram of a file of numbers. Numbers should be floats, one 
per line.

Re-uses sample code and documentation from 
<http://users.soe.ucsc.edu/~karplus/bme205/f12/Scaffold.html>
"""

import argparse, sys, os, itertools, math
import matplotlib
from matplotlib import pyplot

def parse_args(args):
    """
    Takes in the command-line arguments list (args), and returns a nice argparse
    result with fields for all the options.
    Borrows heavily from the argparse documentation examples:
    <http://docs.python.org/library/argparse.html>
    """
    
    # The command line arguments start with the program name, which we don't
    # want to treat as an argument for argparse. So we remove it.
    args = args[1:]
    
    # Construct the parser (which is stored in parser)
    # Module docstring lives in __doc__
    # See http://python-forum.com/pythonforum/viewtopic.php?f=3&t=36847
    # And a formatter class so our examples in the docstring look good. Isn't it
    # convenient how we already wrapped it to 80 characters?
    # See http://docs.python.org/library/argparse.html#formatter-class
    parser = argparse.ArgumentParser(description=__doc__, 
        formatter_class=argparse.RawDescriptionHelpFormatter)
    
    # Now add all the options to it
    parser.add_argument("data", type=argparse.FileType('r'),
        help="the file to read")
    parser.add_argument("--title", default="Histogram",
        help="the plot title")
    parser.add_argument("--x_label", default="Value",
        help="the plot title")
    parser.add_argument("--y_label", default="Number of Items (count)",
        help="the plot title")
    parser.add_argument("--bins", type=int, default=10,
        help="the number of histogram bins")
    parser.add_argument("--min", type=float, default=None,
        help="minimum value allowed")
    parser.add_argument("--max", type=float, default=None,
        help="maximum value allowed")
    parser.add_argument("--font_size", type=int, default=12,
        help="the font size for text")
    parser.add_argument("--cumulative", action="store_true",
        help="plot cumulatively")
    parser.add_argument("--log", action="store_true",
        help="take the logarithm of values before plotting histogram")
    
        
    return parser.parse_args(args)
    

def main(args):
    """
    Parses command line arguments, and plots a histogram.
    "args" specifies the program arguments, with args[0] being the executable
    name. The return value should be used as the program's exit code.
    """
    
    options = parse_args(args) # This holds the nicely-parsed options object
    
    # Read in all the data
    data = [float(line) for line in options.data]
    
    # Strip NaNs and Infs
    data = filter(lambda x: x < float("+inf") and x > float("-inf"), data)
    
    # Apply the limits
    if options.min is not None:
        data = filter(lambda x: x > options.min, data)
    if options.max is not None:
        data = filter(lambda x: x < options.max, data)
        
    if options.log:
        # Plot a log histogram instead
        data = [math.log(value) for value in data]
       
    # Do the plot 
    pyplot.hist(data, options.bins, cumulative=options.cumulative)
    # StackOverflow provides us with font sizing
    # http://stackoverflow.com/questions/3899980/how-to-change-the-font-size-on-a-matplotlib-plot
    matplotlib.rcParams.update({"font.size": options.font_size})
    pyplot.title("{} (n = {})".format(options.title, len(data)))
    pyplot.xlabel(options.x_label)
    pyplot.ylabel(options.y_label)
    if options.min is not None:
        # Set only the lower x limit
        pyplot.xlim((options.min, pyplot.xlim()[1]))
    if options.max is not None:
        # Set only the upper x limit
        pyplot.xlim((pyplot.xlim()[0], options.max))
    pyplot.show()
        
    return 0

if __name__ == "__main__" :
    sys.exit(main(sys.argv))
