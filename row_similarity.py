#!/usr/bin/env python2.7
"""
row_similarity.py: given a TSV file with float values for several
sample rows, produce a sparse similarity matrix.
"""

import tsv, collections, argparse, itertools, sys, numpy

def generate_parser():
    """
    Generate the options parser for this program.
    Borrows heavily from the argparse documentation examples:
    <http://docs.python.org/library/argparse.html>
    """
    
    # Construct the parser (which is stored in parser)
    # Module docstring lives in __doc__
    # See http://python-forum.com/pythonforum/viewtopic.php?f=3&t=36847
    # And a formatter class so our examples in the docstring look good. Isn't it
    # convenient how we already wrapped it to 80 characters?
    # See http://docs.python.org/library/argparse.html#formatter-class
    parser = argparse.ArgumentParser(description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    
    # Now add all the options to it.
    parser.add_argument("categories", type=argparse.FileType("r"), 
        help="TSV of category assignments")
    parser.add_argument("--similarities", type=argparse.FileType("w"),
        default=sys.stdout, 
        help="sparse similarity matrix to write") 
        
    return parser

def parse_args(args):
    """
    Takes in the command-line arguments list (args), and returns a nice argparse
    result with fields for all the options.
    
    """
    
    # The command line arguments start with the program name, which we don't
    # want to treat as an argument for argparse. So we remove it.
    args = args[1:]
    
    # Get the parser
    # parser holds the program's argparse parser.
    parser = generate_parser()
    
    # Invoke the parser
    return parser.parse_args(args)
    
def main(args):
    """
    Parses command line arguments and calculate similarity.
    "args" specifies the program arguments, with args[0] being the executable
    name. The return value should be used as the program's exit code.
    """
    
    options = parse_args(args) # This holds the nicely-parsed options object
    
    # Make a reader for the input matrix
    input_reader = tsv.TsvReader(options.categories)
    
    # Keep a dict of Numpy arrays of values by sample name
    samples = {}
    
    for parts in input_reader:
        # Parse out all the floats for this sample and make an array
        samples[parts[0]] = numpy.array([float(part) for part in parts[1:]])
    
    # Now we need to calculate pairwise similarities by overlap
    # We need a file to put them in. This holds a writer for that.
    similarity_writer = tsv.TsvWriter(options.similarities)
    
    similarity_writer.comment("Similarity matrix for Hexagram Visualization")
    similarity_writer.comment("name 1\tname 2\tsimilarity")
    
    for sample_1, sample_2 in itertools.combinations(samples.iterkeys(), 2):
        # Calculate the similarity as the dot product of the two samples
        similarity = numpy.dot(samples[sample_1], samples[sample_2])
        
        # Now write the sparse similarity matrix entry.
        similarity_writer.line(sample_1, sample_2, similarity)
            
    # Don't close writer because it might be stdout.
    

if __name__ == "__main__" :
    # No error catching because stack traces are important
    # And I don't want to mess around with a module to get them
    sys.exit(main(sys.argv)) 
