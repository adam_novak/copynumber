#!/usr/bin/env python2.7
"""
datawrangler.py: contains the DataWrangler class, which handles extracting
sample CGH ratios from scan text files, getting metadata about each sample, and
getting the array-specific files for samples from an array.

"""

import os, logging, collections, subprocess, operator, itertools, math, sqlite3

import numpy

import tsv
from guppy import hpy
import microarray

class Record(object):
    """
    Represents a metadata record for a sample. Has a "sample" field, an "array"
    field, a "technology" field, and a "condition" field.
    
    """
    
    def __init__(self, sample, array, technology, condition):
        """
        Make a new Record for the given sample ID, run on the given array, with
        the given technology ("cgh" for Agilent Comparative Genome
        Hybridization, or "cnv" for Affymetrix chips with CNV probes) with the
        given condition ("Control, "Microcephaly", or "Macrocephaly").
        
        """
        
        # Store the sample ID
        self.sample = sample
        # Store the array name
        self.array = array
        # Store the technology
        self.technology = technology
        # Store the condition (whether we're a control, micorcephalic, or
        # macrocephalic)
        self.condition = condition
        
    def __str__(self):
        """
        Represent this Record as a human-readable string.
        
        """
        
        return "<Record: {} ({}) on {} ({})>".format(self.sample, 
            self.condition, self.array, self.technology)
            
    def __repr__(self):
        """
        Represent this Record object as executable Python.
        
        """
        
        return "Record(\"{}\", \"{}\", \"{}\", \"{}\")".format(self.sample, 
            self.array, self.technology, self.condition)

class DataWrangler(object):
    """
    A class that can manage CGH sample and array data stored in the following
    hierarchy:
        
        <root>/arrays/<array>.fa
        <root>/alignments/<array>.pslx
        <root>/affinities/<array>.tsv
        <root>/weights/<array>.tsv
        <root>/scans/<array>/<sample>.txt for CGH OR
        <root>/scans/<array>/<sample>.cel for CNV
        <root>/samples/<array>/log<base>/<sample>.tsv for CGH OR
        <root>/samples/<array>/<sample>.tsv for CNV
        <root>/normalized/<array>/<sample>.tsv for CNV
        
    """
    
    def __init__(self, root, index=None):
        """
        Make a new DataWrangler, for data files in the given root path. By
        default, the database index file is read from <root>/index.tsv; this can
        be overridden by passing a stream of <sample ID>\t<array name>\t<sample
        condition> TSV data as the index parameter.
        
        """
        
        # Remember the root
        self.root = root
        
        # We cache our loaded weights files
        self.weights_cache = {}
        
        # We also cache SQLite connections by database filename
        self.connections = {}
        
        if index is None:
            # Load the index from the default location.
            index = open(os.path.join(self.root, "index.tsv"))
        
        # Load the index. This dict holds Record lists indexed by sample.
        self.by_sample = collections.defaultdict(list)
        # This one holds them by array
        self.by_array = collections.defaultdict(list)
        # This one holds them by condition
        self.by_condition = collections.defaultdict(list)
        # TODO: Use a real database or something, and stop totally breaking no
        # forgery.
        
        for sample, array, technology, condition in tsv.TsvReader(index):
            # Make a Record
            record = Record(sample, array, technology, condition)
            
            # Index it
            self.by_sample[sample].append(record)
            self.by_array[array].append(record)
            self.by_condition[condition].append(record)
            
        logging.info("Loaded {} sample records".format(len(self.by_sample)))
        
    def connect(self, database_file):
        """
        Open an SQLite connection to the given database file, or get a cached
        connection. The caller must not close the connection, and must commit
        all their changes.
        
        """
        
        if not self.connections.has_key(database_file):
            self.connections[database_file] = sqlite3.connect(database_file)
            
        return self.connections[database_file]
        
    def scan_path(self, record):
        """
        Return the path to the scan file we should generate for the given
        Record, whether it exists or not.
        
        """
        
        if record.technology == "cgh":
            # Build the path for a CGH scan (.txt)
            return os.path.join(self.root, "scans", record.array,
                "{}.txt".format(record.sample))
        elif record.technology == "cnv":
            # Build the path for a CNV scan (.cel)
            return os.path.join(self.root, "scans", record.array,
                "{}.cel".format(record.sample))
        else:
            raise Exception("Unknown technology: {}".format(record.technology))
        
    
    def find_scan(self, record):
        """
        Return the path to the scan file for this Record, or None if it doesn't
        exist.
        
        """
        
        # Get the path where the scan would be, which is always the best place
        path = self.scan_path(record)
        
        if os.path.exists(path):
            # We found the scan file where it's supposed to be
            logging.debug("Found {} scan at {}".format(record.sample, path))
            return path
        else:
            logging.debug("No {} scan at {}".format(record.sample, path))
            return None
            
    def sample_path(self, record):
        """
        Return the path to the sample file we should generate for the given
        Record, whether it exists or not.
        
        """
        
        
        if record.technology == "cgh":
            # Build the path for a CGH sample (log ratios)
            # We should always output things in log10
            return os.path.join(self.root, "samples", record.array, "log10",
                "{}.tsv".format(record.sample))
        elif record.technology == "cnv":
            # Build the path for a CNV sample (raw intensities)
            return os.path.join(self.root, "samples", record.array,
                "{}.tsv".format(record.sample))
        else:
            raise Exception("Unknown technology: {}".format(record.technology))
    
    def find_sample(self, record):
        """
        Return the path to the sample file for this Record, or None if it
        doesn't exist.
        
        """
        
        if record.technology == "cgh":
            # There are two options here; we could have a log10 sample file or a
            # log2 sample file.
            
            # Build the base path
            base_path = os.path.join(self.root, "samples", record.array)
            
            for log in [10, 2]:
                
                # Build the log-base-specific path
                path = os.path.join(base_path, "log{}".format(log), 
                    "{}.tsv".format(record.sample))
            
                if os.path.exists(path):
                    # We found the scan file where it's supposed to be
                    logging.debug("Found {} sample at {}".format(record.sample, 
                        path))
                    return path
                else:
                    logging.debug("No {} sample at {}".format(record.sample,
                        path))
            
            # If we get here, it wasn't in either place.
            logging.debug("No {} sample found at all.".format(record.sample))
            return None
        elif record.technology == "cnv":
            # There's only one place it can be if it exists.
            path = self.sample_path(record)
            
            if os.path.exists(path):
                logging.debug("Found {} sample at {}".format(record.sample,
                    path))
                return path
            else:
                logging.debug("No {} sample at {}".format(record.sample, path))
                return None
        else:
            raise Exception("Unknown technology: {}".format(record.technology))
            
    def normalized_path(self, record):
        """
        Return the path to the quantile-normalized file we should generate for
        the given Record, whether it exists or not.
        
        This actually happens to be an SQLite database, shared for all records
        on that array.
        
        """
        
        return os.path.join(self.root, "normalized", 
            "{}.sqlite3".format(record.array))
                
    def find_normalized(self, record):
        """
        Return the path to the normalized file for this Record, or None if it
        doesn't exist.
        
        """
        
        # There's only one place it can be if it exists.
        path = self.normalized_path(record)
        
        if os.path.exists(path):
            logging.debug("Found {} sample at {}".format(record.sample,
                path))
            return path
        else:
            logging.debug("No {} sample at {}".format(record.sample, path))
            return None
    
    def get_record(self, sample):
        """
        Return the Record for the given sample ID.
        
        """
        
        # Just get the only item in the list for this sample ID.
        return self.by_sample[sample][0]
        
    def open_sample(self, sample):
        """
        Return a stream of <probe>\t<log ratio> data for the given sample.
        
        If the data has not yet been extracted from the scan file, extract it
        and cache the results.
        
        """
        
        # Get the record for this sample.
        record = self.get_record(sample)
        
        # Get the path to the sample file, if it exists
        sample_path = self.find_sample(record)
        
        if sample_path is None:
            # Extract the file ourselves. Where should we put it?
            sample_path = self.sample_path(record)
            
            logging.info("Creating {}".format(sample_path))
            
            if not os.path.exists(os.path.dirname(sample_path)):
                # Make the necesary directories
                os.makedirs(os.path.dirname(sample_path))
            
            if record.technology == "cgh":
                # We're using an Agilent microarray scan file. Open the
                # microarray scan for reading (before we make our output file,
                # so we don['t make empty outputs if the scan doesn't exist)
                reader = open(self.find_scan(record))
                
                # Open it up for writing in TSV format
                writer = tsv.TsvWriter(open(sample_path, "w"))
                
                for probe_record in microarray.parse(reader):
                    # For each record in the scan file...
                    
                    if not probe_record["ProbeName"].startswith("A_"):
                        # Skip probes that aren't really measuring things (i.e.
                        # controls)
                        continue
                    
                    # Write the probe and the log ratio. This is log10, so the
                    # default placement of the sample file in the log10
                    # directory is consistent with this.
                    writer.line(probe_record["ProbeName"], 
                        probe_record["LogRatio"])
                
                writer.close()
            elif record.technology == "cnv":
                # We need to extract the CNV probes from an Affymetrix cel file.
                # Use the R script.
                subprocess.check_call(["./extract_expression.R", 
                    self.find_scan(record), sample_path])
            else:
                raise Exception("Unknown technology: {}".format(
                    record.technology))
        
        else:
            # We already have the sample
            logging.info("Opening sample {}".format(sample))
                
            
        # Open up the sample file and return the handle
        return open(sample_path)
        
    def load_sample(self, sample):
        """
        Given a smaple ID, load the raw, unweighted, un-normalized intensity or
        sample/normal ratio values.
        
        If the sample is actually stored as log ratios, un-log them.
        
        Retuns a dict of those values as floats, by probe name.
        
        """
        
        # Determine if we need to un-log or not. This holds the log base to un-
        # log with, or None if we don't need to un-log.
        log_base = None
        
        if self.get_technology(sample) == "cgh":
            # We do need to un-log.
            log_base = self.get_base(sample)
        
        # This will hold the measured probe values (as floats) by
        # probe name
        values = {}

        # Read in the values from the stream that we get for the sample.
        for parts in tsv.TsvReader(self.open_sample(sample)):
            # We get probe name as a string, and probe value as a
            # string.
            if len(parts) < 2:
                # Skip blank lines and what have you
                continue
                
            if parts[1] == "":
                # Empty value means it wasn't measured. Skip it.
                continue
            
            # Save the value as a float
            values[parts[0]] = float(parts[1])
            
            if log_base is not None:
                # We read the data in log format. Un-log it.
                try:
                    values[parts[0]] = math.pow(log_base, values[parts[0]])
                except OverflowError:
                    # The ratio is really some absurd number that makes no sense
                    # as a log ratio. Throw it out.
                    del values[parts[0]]
                    logging.error("Log ratio {} for {} is too big".format(
                        parts[1], parts[0]))
            
        return values
    
    def quantile_normalize(self, array):
        """
        Do quantile normalization for the given array, saving the resulting
        normalized probe intensities for each sample and probe in the
        appropriate SQLite database.
        
        """
        
        # Make a list of all the samples that need to go into our normalization
        to_normalize = list(self.get_samples(array))
        
        logging.info("Quantile normalizing {} samples for array {}".format(
            len(to_normalize), array))
            
        if len(to_normalize) == 0:
            # Don't try and normalize nothing.
            raise Exception("Can't normalize 0 samples")
            
        # Where should the normalized samples go?
        path = self.normalized_path(self.get_record(to_normalize[0]))
        
        # Open up the database
        connection = self.connect(path)
        
        # Get a cursor
        cursor = connection.cursor()
        
        # Make the table
        cursor.execute("CREATE TABLE IF NOT EXISTS normalized ("
            "sample TEXT NOT NULL, "
            "probe TEXT NOT NULL, "
            "value REAL NOT NULL, "
            "PRIMARY KEY (sample, probe))")
        # Empty it of old data
        cursor.execute("DELETE FROM normalized")
        
        # This holds a dict of Numpy structured arrays with "probe" strings and
        # "intensity" values. Those Numpy arrays are sorted by intensity.
        intensity_data = {}
        
        for i, sample in enumerate(to_normalize):
            # Load the sample as a probe, value dict.
            # This means we pick the last entry for repeated probes
            sample_probe_intensities = {probe: float(value) for probe, value in
                tsv.TsvReader(self.open_sample(sample))}
                
            # Work out how long the longest probe name is. We need this for our
            # structured array.
            longest_probe_name = max(len(key) for key in
                sample_probe_intensities.iterkeys())
            
            # Determine the data type of the structured array we want to
            # construct: a string long enough to hold the longest probe name,
            # and a float. See <http://stackoverflow.com/a/12874032/402891>.
            # Note: every file needs to contain the same set of probes for us to
            # get structured arrays of the same type for each sample.
            data_type = [("probe", (str, longest_probe_name)), ("intensity", 
                float)] 
            
            # Make the structured array from the dict.
            intensity_data[sample] = numpy.fromiter(
                sample_probe_intensities.iteritems(), dtype=data_type)
                
            # Explicitly throw out the dict so we don't keep the last one around
            # for the whole function.
            del sample_probe_intensities
                
            # Sort the structured array by intensity, in place.
            intensity_data[sample].sort(order="intensity")
            
            # Log memory usage
            logging.info("Memory usage after loading {} ({}): {}".format(sample,
                i, hpy().heap().size))
                
        
        logging.info("Running normalization on {} rows...".format(len(
            intensity_data.values()[0])))
            
        for row in itertools.izip(*intensity_data.values()):
            # We have the probe and its value at rank n for each sample, as a
            # sequence of individual structured array views. Find the mean.
            mean = sum((record["intensity"] for record in row)) / len(row)
            
            # Set each record's intensity to the mean for its rank.
            for record in row:
                record["intensity"] = mean
        
        for normalized_sample, normalized_data in intensity_data.iteritems():
            
            # Now save all the normalized arrays
            logging.info("Saving sample {}".format(normalized_sample))
            for record in normalized_data:
                # Write each normalized value.
                cursor.execute("REPLACE INTO normalized VALUES (?,?,?)", 
                    (normalized_sample, record["probe"], record["intensity"]))
                
            # Actually write to disk, so we don't need to do it all at once.
            connection.commit()
        
    def open_normalized(self, sample):
        """
        Return a stream of <probe>\t<normalized value> TSV data for the given
        sample. If the data files for the sample's array have not yet been
        quantile-normalizd, quantile-normalize them first.
        
        """
        
        # TODO: Ban cgh arrays from quantile normalization.
        
        # Go get where the normalized version should be, in case we've done it
        # already.
        path = self.find_normalized(self.get_record(sample))
        
        if path is not None:
            logging.debug("Found sample {} pre-normalized.".format(sample))
            # Already did all the work, just read the cached result
            return open(path)
            
        # If we get here, we need to do the normalization.
        self.quantile_normalize(self.get_array(sample))
            
        # Now we know the normalized version exists. Recurse.
        return self.open_normalized(sample)
        
    def load_normalized(self, sample):
        """
        Given a CNV sample ID, load the normalized intensity values for it.
        Returns a dict of normalized intensity floats by probe.
        
        """
        
        # Go get where the normalized version should be, in case we've done it
        # already.
        path = self.find_normalized(self.get_record(sample))
        
        if path is None:
            # We need to normalize the samples on the array first
            self.quantile_normalize(self.get_array(sample))
            
        # Connect to the database
        connection = self.connect(path)
        
        # Get a cursor
        cursor = connection.cursor()
        
        # This will hold the measured probe intensities (as floats) by
        # probe name
        measured_intensities = {}

        # Read in the quantile-normalized probe hybridizations from the stream
        # that we get for the sample.
        for probe, intensity in cursor.execute("SELECT probe, value "
            "FROM normalized WHERE sample=?", (sample,)):
            
            # Save the intensity as a float
            measured_intensities[probe] = intensity
            
        return measured_intensities
        
    def get_normalized(self, sample, probe):
        """
        Return the normalized intensity for the given array, sample, and probe.
        
        Internally, uses a cached SQLite database connection per array.
        
        """
        
        # What array is this sample from?
        array = self.get_array(sample)
        
        # Go get where the normalized sample lives (in the per-array database)
        path = self.find_normalized(self.get_record(sample))
        
        if path is None:
            # We need to normalize the samples on the array first
            self.quantile_normalize(array)
            
        # Connect to the database
        connection = self.connect(path)
        
        # Get a cursor
        cursor = connection.cursor()
            
        for intensity in cursor.execute("SELECT value FROM normalized "
            "WHERE sample=? AND probe=?", (sample, probe)):
            
            # We found it
            return intensity
            
    def load_weighted_ratios(self, sample):
        """
        Given a sample ID, load its weighted sample/normal ratios (with quantile
        normalization for CNV samples). Returns a dict of sample/normal ratio by
        probe name.
        
        Do not use with CNV arrays that do not have weights; it will produce
        absurdly huge ratios.
        
        """
        
        # Load the weights for the array first; a dict of weight by probe,
        # defaulting to 1. Note that this is a terrible default for CNV samples,
        # which rely on the weight really being the reciprocal of an average
        # intensity.
        weights = self.load_weights(self.get_array(sample))
        
        if self.get_technology(sample) == "cnv":
            # Load the things that need to be weighted: quantile normalized CNV
            # intensities by sample.
            to_weight = self.load_normalized(sample)
        else:
            # Load CGH sample/normal ratios without any weighting or
            # normalization applied yet.
            to_weight = self.load_sample(sample)
            
        for probe, weight in weights.iteritems():
            if to_weight.has_key(probe):
                # Apply each weight that isn't just the dict's default of 1, in
                # place.
                to_weight[probe] *= weight
                
        # All weights have been applied. Since the CNV weights are the
        # reciprocals of normal intensities, we have sample/normal ratios, which
        # is what we're supposed to get.
        return to_weight
                
        
        
    def get_base(self, sample):
        """
        Given a sample ID, return the logarithm base for that sample, or None if
        the sample doesn't have an extracted sample file yet.
        
        TODO: This is kind of a hack; it works by interrogating the filesystem.
        Put log base in the database or something.
        
        """
        
        # Get the record for the sample
        record = self.get_record(sample)
        
        if record.technology != "cgh":
            raise Exception("{} record {} has no log base".format(
                record.technology, record.sample))
        
        # Build the base path
        base_path = os.path.join(self.root, "samples", record.array)
        
        for log in [10, 2]:
            
            # Build the log-base-specific path
            path = os.path.join(base_path, "log{}".format(log), 
                "{}.tsv".format(record.sample))
        
            if os.path.exists(path):
                # We found the scan file under this log base
                return log
                
        # We didn't find the scan file
        return None
        
    def get_array(self, sample):
        """
        Return the array name for the given sample. This is so that outside
        things never have to deal with Records.
        
        """
        
        return self.get_record(sample).array
        
    def get_condition(self, sample):
        """
        Return the condition name for the given sample. This is so that outside
        things never have to deal with Records.
        
        """
        
        return self.get_record(sample).condition
        
    def get_technology(self, sample):
        """
        Return the technology for the given sample. This is so that outside
        things never have to deal with Records.
        
        """
        
        return self.get_record(sample).technology
        
        
    def get_samples(self, array, condition=None):   
        """
        Iterate through all samples for the given array. If condition is
        specified, only yield samples with that condition.
        
        Yields sample IDs
        
        """
        
        for record in self.by_array[array]:
            if condition is not None and record.condition != condition:
                # This sample does not match our condition filter
                continue
            
            # Yield the sample ID.
            yield record.sample
        
    def weights_path(self, array):
        """
        Get the path to the weights file for an array, whether it exists or not.
        
        The weights file holds <probe>\t<weight> TSV records.
        
        """
        
        # It's just <array>.tsv under the "weights" directory.
        return os.path.join(self.root, "weights", "{}.tsv".format(array))
    
    def load_weights(self, array):
        """
        Load the weights for an array.
        
        Returns a defaultdict of either 1 or the weight for each probe, should
        that weight exist. Caller may not modify the defaultdict returned.
        
        """
        
        # This will hold the reciprocal average intensities (i.e. weights) by
        # probe name
        weights = collections.defaultdict(lambda: 1)
        
        # This holds the path to the corrective per-probe weights for the array
        weights_path = self.weights_path(array)
        
        if self.weights_cache.has_key(weights_path):
            # We already loaded these weights
            return self.weights_cache[weights_path]
        
        if os.path.exists(weights_path):
            logging.info("Loading weights from {}".format(weights_path))
            # Read in the weights
            for parts in tsv.TsvReader(open(weights_path)):
                # We get probe name as a string, and probe intensity as a
                # string.
                if len(parts) < 2:
                    # Skip blank lines and what have you
                    continue
                
                # Save the weight as a float
                weights[parts[0]] = float(parts[1])
            
            # Cache the weights
            self.weights_cache[weights_path] = weights
                
        else:
            logging.info("No weights available in {}".format(weights_path))
                
        return weights
        
    def affinities_path(self, array):
        """
        Get the path to the affinities file for the array, whether it exists or
        not.
        
        The affinities file holds <probe>\t<contig>\t<start>\t<stop>\t<affinity>
        TSV records.
        
        """
        
        # It's just <array>.tsv under the "affinities" directory.
        return os.path.join(self.root, "affinities", "{}.tsv".format(array))
        
    def open_affinities(self, array):
        """
        Return a stream of probe-affinity TSV data for the given array. The
        array's affinities file must exist.
        
        """
        
        return open(self.affinities_path(array))
        
        
        
            
            
        
        
        
        
        
