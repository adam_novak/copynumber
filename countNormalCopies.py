#!/usr/bin/env python2.7
"""
countNormalCopies.py: Count copies of probes in "normal" reads.

Re-uses sample code and documentation from 
<http://users.soe.ucsc.edu/~karplus/bme205/f12/Scaffold.html>
"""

import argparse, sys, os, collections, math, itertools

import tsv
import regex
from Bio import SeqIO

def parse_args(args):
    """
    Takes in the command-line arguments list (args), and returns a nice argparse
    result with fields for all the options.
    
    Borrows heavily from the argparse documentation examples:
    <http://docs.python.org/library/argparse.html>
    """
    
    # Construct the parser (which is stored in parser)
    # Module docstring lives in __doc__
    # See http://python-forum.com/pythonforum/viewtopic.php?f=3&t=36847
    # And a formatter class so our examples in the docstring look good. Isn't it
    # convenient how we already wrapped it to 80 characters?
    # See http://docs.python.org/library/argparse.html#formatter-class
    parser = argparse.ArgumentParser(description=__doc__, 
        formatter_class=argparse.RawDescriptionHelpFormatter)
    
    # General options
    parser.add_argument("probe_sequences", type=argparse.FileType("r"),
        help="FASTA file of probe sequences")
    parser.add_argument("normal_reads", type=argparse.FileType("r"),
        help="FASTQ file of reads to search")
    parser.add_argument("--out_file", type=argparse.FileType("w"),
        default=sys.stdout,
        help="output TSV of sequences and counts for probe-matching sequences")
    parser.add_argument("--threshold", type=int, default=5,
        help="maximum number of mismatches to tolerate")
        
    # The command line arguments start with the program name, which we don't
    # want to treat as an argument for argparse. So we remove it.
    args = args[1:]
        
    return parser.parse_args(args)

def affinity(probe, target):
    """
    Given two DNA sequences, return the probe's affinity for the reverse
    complement of the target as a fraction of its affinity for a perfect match
    sequence.
    
    """
    
    # Count up the mismatches
    mismatches = 0
    
    for a, b in itertools.izip(probe, target):
        if a != b:
            mismatches += 1
    
    # TODO: Work out real math for this.
    return 1.0 - 0.1 * mismatches

def main(args):
    """
    Parses command line arguments and do the work of the program.
    "args" specifies the program arguments, with args[0] being the executable
    name. The return value should be used as the program's exit code.
    """
    
    options = parse_args(args) # This holds the nicely-parsed options object

    # This will format our output table of <probe name>\t<affinity> for each
    # close match.
    writer = tsv.TsvWriter(options.out_file)

    # Load all the probes
    probes = SeqIO.to_dict(SeqIO.parse(options.probe_sequences, "fasta"))
    
    # Make a regex for each probe
    probe_regexes = {probe_name: regex.compile("(?e)({}){{s<={}}}".format(
        probe_sequence.seq, options.threshold)) for probe_name, probe_sequence 
        in probes.iteritems()}
    
    print "Made {} probe regexes".format(len(probe_regexes))
    
    # This holds total affinity by probe name
    total_affinities = {probe_name: 0.0 for probe_name in probes.iterkeys()}
    
    # This holds the number of the read
    
    for read in SeqIO.parse(options.normal_reads, "fastq"):
        for probe_name, probe_regex in probe_regexes.iteritems():
            
            # Get the best fit match, or None
            match = regex.search(probe_regex, str(read.seq))
            
            if match is not None:
                
                # Get the affinity for this read
                read_affinity = affinity(probes[probe_name].seq, match[0])
                
                # Add to the total affinity
                total_affinities[probe_name] += read_affinity
                
                print "Total for {}: {}".format(probe_name, 
                    total_affinities[probe_name])
                
            
    
    for probe_name, total_affinity in total_affinities.iteritems():                
        # Write out the total affinity for each probe.
        writer.line(probe_name, total_affinity)
    
    
    

if __name__ == "__main__" :
    sys.exit(main(sys.argv))
