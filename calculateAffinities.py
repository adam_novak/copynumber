#!/usr/bin/env python2.7
"""
calculateAffinities.py: Make a TSV with probe affinities for mapping locations from a
FASTA of probes and a PSLX of probe mapping locations.

Re-uses sample code and documentation from 
<http://users.soe.ucsc.edu/~karplus/bme205/f12/Scaffold.html>
"""

import argparse, sys, os, collections, math, itertools, logging

import tsv
from Bio import SeqIO, SearchIO
import rpy2.robjects

def parse_args(args):
    """
    Takes in the command-line arguments list (args), and returns a nice argparse
    result with fields for all the options.
    
    Borrows heavily from the argparse documentation examples:
    <http://docs.python.org/library/argparse.html>
    """
    
    # Construct the parser (which is stored in parser)
    # Module docstring lives in __doc__
    # See http://python-forum.com/pythonforum/viewtopic.php?f=3&t=36847
    # And a formatter class so our examples in the docstring look good. Isn't it
    # convenient how we already wrapped it to 80 characters?
    # See http://docs.python.org/library/argparse.html#formatter-class
    parser = argparse.ArgumentParser(description=__doc__, 
        formatter_class=argparse.RawDescriptionHelpFormatter)
    
    # General options
    parser.add_argument("probe_sequences", type=argparse.FileType("r"),
        help="FASTA file of probe sequences")
    parser.add_argument("mapping_pslx",
        help="PSLX file mapping probes to the reference genome, with sequence")
    parser.add_argument("--bed", type=argparse.FileType("r"), default=None,
        help="BED file to restrict analysis to")
    parser.add_argument("--out_file", type=argparse.FileType("w"),
        default=sys.stdout,
        help="output TSV of probes and mapping location affinities")
    parser.add_argument("--threshold", type=int, default=5,
        help="maximum number of mismatches to tolerate")
        
    # Logging options
    parser.add_argument("--loglevel", default="DEBUG", choices=["DEBUG", "INFO",
        "WARNING", "ERROR", "CRITICAL"],
        help="logging level to use")
        
    # The command line arguments start with the program name, which we don't
    # want to treat as an argument for argparse. So we remove it.
    args = args[1:]
        
    return parser.parse_args(args)
    
def set_loglevel(loglevel):
    """
    Given a string log level name, set the logging module's log level to that
    level. Raise an exception if the log level is invalid.
    
    """
    
    # Borrows heavily from (actually is a straight copy of) the recipe at
    # <http://docs.python.org/2/howto/logging.html>
    
    # What's the log level number from the module (or None if not found)?
    numeric_level = getattr(logging, loglevel.upper(), None)
    
    if not isinstance(numeric_level, int):
        # Turns out not to be a valid log level
        raise ValueError("Invalid log level: {}".format(loglevel))
    
    # Set the log level to the correct numeric value
    logging.basicConfig(format="%(levelname)s:%(message)s", level=numeric_level)

def affinity(probe, probe_subsequence, target):
    """
    Given a complete probe BioPython SeqRecord, and two aligned probe and target
    BioPython SeqRedcords (for the same conceptual strand), return the probe
    subsequence's propensity to bind for the target sequence as a fraction of
    the whole probe's propensity to bind to a perfect match sequence.
    
    The DECIPHER R package must already have been loaded into rpy2's R instance.
    This is done with the initialize_rpy2() function below, which myst be called
    before this function will work.
    
    """
    
    # Get the string for the probe
    probe_string = str(probe.seq)
    
    # Get the string for the part of the probe that actually binds the target
    probe_subsequence_string = str(probe_subsequence.seq)
    
    # Get the string for the target
    target_string = str(target.seq)
    
    if (probe_string == target_string and 
        probe_string == probe_subsequence_string):
        # Shortcut: we know the affinity of a perfect match on the whole probe
        # is 1.
        return 1.0
        
    # Grab the hybridization efficiency function, which returns a sequence of
    # efficiency, deltaG
    efficiency = rpy2.robjects.r["CalculateEfficiencyArray"]
    
    # Get the hybridization efficiency with a perfect match of the whole probe
    self_efficiency, _ = efficiency(probe_string, probe_string)
    
    # Get the hybridization efficiency of the probe subsequence with the target
    target_efficiency, _ = efficiency(probe_subsequence_string, target_string)
    
    # Return the affinity. TODO: Is this semantically correct?
    return target_efficiency / self_efficiency
    
def initialize_rpy2():
    """
    Set up rpy2 with the DECIPHER library. Must be called before affinity() will
    work.
    
    """
    
    # Load the DECIPHER R package.
    logging.info("Initializing DECIPHER")
    try:
        rpy2.robjects.r("library(DECIPHER)")
    except rpy2.rinterface.RRuntimeError:
        # Try installing DECIPHER
        logging.warning("Couldn't load DECIPHER. Trying to install it.")
        
        rpy2.robjects.r("source(\"http://bioconductor.org/biocLite.R\")")
        rpy2.robjects.r("biocLite(\"DECIPHER\")")
        
        logging.info("Initializing DECIPHER")
        rpy2.robjects.r("library(DECIPHER)")

def filter_pslx(pslx_filename, max_mismatches=5, probes_to_use=None, 
    probe_length=None):
    """
    Given a PSLX filename, get all sufficiently good High Scoring Pairs (HSPs).
    
    HSPs are sufficiently good if they are gapless, unfragmented, and have
    max_mismatches or fewer mismatches.
    
    If specified, probes_to_use gives an iterable of probe IDs to look up in the
    PSLX; otherwise, all probes in the PSLX are considered.
    
    If specified, probe_length gives the length of the query sequences used; any
    bases that were supposedly in the query sequence but don't show up in the
    HSP at all will be counted as mismatches.
    
    Yields query sequence IDs and lists of HSPs, where each list contains all
    the sufficiently good HSPs for that query.
    
    """
    
    logging.info("Getting PSLX Index")
    pslx = SearchIO.index(pslx_filename, "blat-psl", pslx=True)
    
    logging.info("{} queries with results in PSLX".format(len(pslx)))
    
    if probes_to_use is not None:
        # Use only the queries with IDs in the list passed
        queries = (pslx[probe] for probe in probes_to_use)
        # How many will there be?
        total_queries = len(probes_to_use)
    else:
        # Use all the queries in the PSLX
        queries = pslx.itervalues()
        # How many queries will there be?
        total_queries = len(pslx)
        
    logging.info("Loading {} queries".format(total_queries))
    
    for i, query in enumerate(queries):
        
        logging.debug("Processing query {}/{}".format(i, total_queries))
        
        # This holds all the sufficiently good HSPs for the query. We assume
        # queries are only yielded once (and you didn't do something silly like
        # cat two PSLX files together).
        query_hsps = []
        for hit in query:
            for hsp in hit:
                # For each high scoring pair of aligned regions (i.e. probe
                # mapping location)
                
                if hsp.is_fragmented:
                    # Skip HSPs broken up into multiple blocks on gaps
                    continue
                    
                if hsp.hit_span != hsp.query_span:
                    # One thing has more bases than the other. Skip it.
                    continue
                
                if hsp.gap_num > 0 or hsp.mismatch_num > max_mismatches:
                    # Too many gaps or mismatches
                    continue
                    
                if probe_length is not None and hsp.mismatch_num + \
                    (probe_length - hsp.query_span) > max_mismatches:
                
                    # Too many mismatches and trailing-off-the-end unaligned
                    # bases.
                    continue
                    
                # This HSP is good enough. Put it in the list.
                query_hsps.append(hsp)
        
        # Now we have all the sufficiently good HSPs for this query. Yield the
        # query sequence ID, and its list of HSPs.
        yield query.id, query_hsps

def main(args):
    """
    Parses command line arguments and do the work of the program.
    "args" specifies the program arguments, with args[0] being the executable
    name. The return value should be used as the program's exit code.
    """
    
    options = parse_args(args) # This holds the nicely-parsed options object

    # Set up logging
    set_loglevel(options.loglevel)

    # Load up rpy2 with the libraries we need
    initialize_rpy2()

    # This will format our output table of <probe name>\t<contig>\t<start>\t
    # <end>\t<affinity> for each close match.
    writer = tsv.TsvWriter(options.out_file)

    # Load all the probes
    probes = SeqIO.to_dict(SeqIO.parse(options.probe_sequences, "fasta"))
    
    # Read the first probe sequence and use it to guess the probe length that
    # all probes ought to be.
    probe_length = len(probes.itervalues().next())
    logging.info("Detected probe length as {} bp".format(probe_length))
    
    if options.bed is not None:
        # Read in the BED of regions that probes must map in to be used
        logging.info("Reading Region of Interest BED")
        
        # This holds all the BED regions as (contig, start, end) tuples.
        bed_regions = [(parts[0], int(parts[1]), int(parts[2])) for parts in 
            tsv.TsvReader(options.bed)]
    
    for query, hsp_list in filter_pslx(options.mapping_pslx, 
        max_mismatches=options.threshold, probe_length=probe_length):
        
        # For each probe and its list of sufficiently good HSPs
        
        if options.bed is not None:
            # Check to make sure that at least one of these HSPs overlaps a BED
            # region.
            
            # Start out by assuming that no HSP overlaps any BED region.
            overlap = False
            
            # TODO: this is O(n^2)
            for contig, start, end in bed_regions:
                for hsp in hsp_list:
                    if (contig == hsp.hit_id and hsp.hit_start < end and 
                        hsp.hit_end > start):
                        
                        # The HSP overlaps this BED region.
                        overlap = True
                        break
                
                if overlap:
                    # Already found an overlap, so stop searching.
                    break
            
            if not overlap:
                # No HSP from this probe overlapped any BED region. The probe is
                # completely uninformative about the region we're interested in,
                # so throw it out.
                continue
                
        # If we get here, at least one mapping location for the probe is in the
        # BED region set, or we don't have any BED to restrict to.
        
        for hsp in hsp_list:
            # For each HSP
            
            if probes.has_key(query):
                # Only use probes we actually have sequence for.
            
                # Compute the affinity of the probe to this location
                mapping_affinity = affinity(probes[query], hsp.query, hsp.hit)
                
                # Write out the probe, the mapping location, and the affinity.
                writer.line(query, hsp.hit_id, hsp.hit_start, hsp.hit_end, 
                    mapping_affinity)
    
    
    
    

if __name__ == "__main__" :
    sys.exit(main(sys.argv))
