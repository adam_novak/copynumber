#!/usr/bin/env python2.7
"""
step: fit a step function to data. Numbers should be floats, two per line. Plots
a scatter plot after the fit is done.

See <http://stackoverflow.com/questions/1514484/fitting-a-step-function>.

Re-uses sample code and documentation from 
<http://users.soe.ucsc.edu/~karplus/bme205/f12/Scaffold.html>
"""

import argparse, sys, os, itertools, math, itertools, functools
import matplotlib
from matplotlib import pyplot

import numpy, scipy.optimize

def parse_args(args):
    """
    Takes in the command-line arguments list (args), and returns a nice argparse
    result with fields for all the options.
    Borrows heavily from the argparse documentation examples:
    <http://docs.python.org/library/argparse.html>
    """
    
    # The command line arguments start with the program name, which we don't
    # want to treat as an argument for argparse. So we remove it.
    args = args[1:]
    
    # Construct the parser (which is stored in parser)
    # Module docstring lives in __doc__
    # See http://python-forum.com/pythonforum/viewtopic.php?f=3&t=36847
    # And a formatter class so our examples in the docstring look good. Isn't it
    # convenient how we already wrapped it to 80 characters?
    # See http://docs.python.org/library/argparse.html#formatter-class
    parser = argparse.ArgumentParser(description=__doc__, 
        formatter_class=argparse.RawDescriptionHelpFormatter)
    
    # Now add all the options to it
    parser.add_argument("data", type=argparse.FileType('r'),
        help="the file to read")
    parser.add_argument("--title", default="Scatterplot",
        help="the plot title")
    parser.add_argument("--x_label", default="X",
        help="the plot title")
    parser.add_argument("--y_label", default="Y",
        help="the plot title")
    parser.add_argument("--font_size", type=int, default=12,
        help="the font size for text")
        
    return parser.parse_args(args)

def step_function(num_steps, steps, levels, x):
    """
    Given a number of steps, a sequence of n step positions, and a sequence of
    n+1 function values, evaluate a step function on the given input, which must
    be a Numpy array. More or less copied straight from
    <http://stackoverflow.com/q/1514484/402891>.
    
    """
    
    # Make an array to hold the result
    y = numpy.zeros(x.shape)
    
    # Everything before the end of the first step gets the first step's value.
    y[x < steps[0]] = levels[0]
    
    for i in xrange(1, num_steps):
        # Which positions are above the last step?
        above_last_step = (x >= steps[i - 1])
        
        # Which positions are below the next step?
        below_next_step = (x < steps[i])
        
        # Every x position that's above or at the end of the previous step but
        # below the end of this step gets this step's value.
        y[numpy.logical_and(above_last_step, below_next_step)] = levels[i]
            
    # Everything at or after the end of the last step gets the very last value
    # (not really associated with a step).
    y[x >= steps[num_steps - 1]] = levels[num_steps]
    
    if numpy.any(y == 0):
        raise Exception("Zero escaped being set!")
    
    # Return the result.
    return y
    
def step_error_function(num_steps, steps, levels, x, y):
    """
    Given a number of steps, a sequence of n step positions, a sequence of n+1
    function values, a Numpy array of x positions, and a Numpy array y values we
    are trying to fit, return a Numpy array of per-point errors.
    
    """
    
    # See <http://stackoverflow.com/q/1514484/402891>. Just take the difference.
    return step_function(num_steps, steps, levels, x) - y

def splits(count, start, end):
    """
    Given a number of split points and the indices of the first and last items
    in a list to split between, yield all possible locations for those split
    points in that list of items. A split point of n means split before the nth
    element of the sequence. Split points are yoielded as a list.
    
    """
    
    if count == 1:
        # This is easy: run through all internal positions, start to end - 1
        for location in xrange(start + 1, end):
            yield [location]
    else:
        for first_locations in splits(1, start, end - (count - 1)):
            # For each first split location that has enough room after it for
            # count-1 other split locations... (these are 1-element lists)
            
            for other_locations in splits(count-1, first_locations[0], end):
                # For each way to split the part that we just split off
                yield first_locations + other_locations
                
def fit_step_function(num_steps, x_data, y_data):
    """
    Given a number of steps, return the split points, levels, and error of the
    best-fit step function for x_data as a function of y_data.
    
    """
    
    print "Fitting with {} steps...".format(num_steps)
    
    # Sort the points in x
    x_sorted = numpy.sort(x_data)
    
    # This holds all the x positions to try for the discontinuity, as a sorted,
    # uniquified list. Only consider unique positions between points, excluding
    # positions shared by two points (which, since we have to assign those
    # points to one side or the other anyway, makes no difference). Don't
    # consider positions before the first point or after the last.
    discontinuity_positions = sorted({(a + b) / 2.0 for a, b in itertools.izip(
        x_sorted, x_sorted[1:]) if a != b})
        
    # This holds the best step positions for the step function
    step_best_steps = None
    # This holds the best levels for the step fit
    step_best_levels = None
    # And this holds the error for it
    step_best_error = float("+inf")

    for discontinuity_indices in splits(num_steps, 0, 
        len(discontinuity_positions)):
    
        # For every list of places to put the split points...
        
        # Where do the split points actually go, coordiante-wise?
        split_positions = [discontinuity_positions[index] for index in 
            discontinuity_indices]
            
        # Fit the step function starting at these discontinuity positions
    
        # This holds starting step levels. Make some not-absurd guesss at them.
        step_levels = [numpy.mean(y_data)] * (num_steps + 1)
        
        # Do the fit and see if it worked
        step_levels, success = scipy.optimize.leastsq(functools.partial(
            step_error_function, num_steps, split_positions), step_levels, 
            args=(x_data, y_data))
            
        if not success:
            # Something broke in the fit
            raise Exception("Failed to fit step function.")
            
        # What's the error (sum squared error)
        step_error = numpy.sum(step_error_function(num_steps, split_positions, 
            step_levels, x_data, y_data) ** 2)
            
        if step_error < step_best_error:
            # We found a new best fit.
            step_best_error = step_error
            step_best_levels = step_levels
            step_best_steps = split_positions
        
    return step_best_steps, step_best_levels, step_best_error

def main(args):
    """
    Parses command line arguments, and plots a histogram.
    "args" specifies the program arguments, with args[0] being the executable
    name. The return value should be used as the program's exit code.
    """
    
    options = parse_args(args) # This holds the nicely-parsed options object
    
    # This holds the X coordinates for each data point. Starts out as a list,
    # but gets converted to a Numpy array.
    x_data = []
    # And this holds the Y coordinates
    y_data = []
    
    for line in options.data:
        if line.startswith("#"):
            # Skip comments
            continue
        
        # Unpack and parse the two numbers on this line
        parts = map(float, line.split())
        
        # Put each in the appropriate list.
        x_data.append(parts[0])
        y_data.append(parts[1])
    
    # Convert to arrays
    x_data = numpy.array(x_data)
    y_data = numpy.array(y_data)
    
    # Do the fits.
    
    # This holds the numbers of splits we want to look at
    step_counts = []
    
    # Do all the fits
    results = [fit_step_function(i, x_data, y_data) for i in step_counts]
    # Pull out the fit split location lists (first return values)
    split_lists = [entry[0] for entry in results]
    # Pull out the fit level parameter Numpy arrays (second return values)
    level_arrays = [entry[1] for entry in results]
    # Pull out the summed square derrors (third return values)
    errors = [entry[2] for entry in results]
    
    # Use these colors to draw the fits:
    colors = ['g', 'y', 'c']
    
    # Prepare the axes so they actually look good.
    
    # Make a figure
    figure = pyplot.figure()
    
    # Make some axes (a single subplot)
    axes = figure.add_subplot(1, 1, 1)
    
    # Do the plot
    axes.scatter(x_data, y_data)
    # StackOverflow provides us with font sizing
    # <http://stackoverflow.com/q/3899980/402891>
    matplotlib.rcParams.update({"font.size": options.font_size})
    axes.set_title("{} (n = {})".format(options.title, len(x_data)))
    axes.set_xlabel(options.x_label)
    axes.set_ylabel(options.y_label)
    
    # This holds a set of available X tick locations
    x_tick_set = {0}
    # And this a set of available Y tick locations
    y_tick_set = {0}
    
    for num_steps, step_positions, step_values, error, color in itertools.izip(
        step_counts, split_lists, level_arrays, errors, colors):
    
        # Talk about the step function
        print "Step function with {} steps, error {:.4f}".format(num_steps,
            error)
        print "\tStarts at {:.4f}".format(step_values[0])
        for i in xrange(num_steps):
            print("\tChanges at {:.4f} to {:.4f}".format(step_positions[i], 
                step_values[i+1]))
    
        # Draw the step function
        
        # What X data do we need to pass to step? It's kind of unintuitive so I
        # just messed with this until the plot came out right.
        step_x = [numpy.min(x_data)] + list(step_positions) + \
            [numpy.max(x_data)]
        # And what Y data goes with it?
        step_y = [step_values[0]] + list(step_values)

        # Draw the steps
        axes.step(step_x, step_y, color=color)
        
        for position in step_positions:
            # Potentially have a tick at each step
            x_tick_set.add(position)
            
        for value in step_values:
            # And at each level
            y_tick_set.add(value)
    
    # Set up the axes to have a few ticks
    axes.xaxis.set_major_locator(pyplot.FixedLocator([0, 1, 2, 3, 4]))
    axes.yaxis.set_major_locator(pyplot.MaxNLocator(nbins=4))
    
    # Set the x bounds to fit the data exactly.
    #pyplot.xlim((numpy.min(x_data), numpy.max(x_data)))
        
    # Flip the time axis to be time-wise. See
    # <http://stackoverflow.com/a/8280500/402891>
    axes.invert_xaxis()
    
    figure.tight_layout()
    
    pyplot.show()
        
    return 0

if __name__ == "__main__" :
    sys.exit(main(sys.argv))
