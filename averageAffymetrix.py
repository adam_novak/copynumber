#!/usr/bin/env python2.7
"""
averageAfftymetrix.py: Average the CNV probe intensities of several Affymetrix
SNP6 CEL files.

Produces output of the form <probe name>\t<average>\t<standard deviation>

Re-uses sample code and documentation from 
<http://users.soe.ucsc.edu/~karplus/bme205/f12/Scaffold.html>
"""

import argparse, sys, os, collections, math, itertools, subprocess, tempfile
import logging

import tsv

def parse_args(args):
    """
    Takes in the command-line arguments list (args), and returns a nice argparse
    result with fields for all the options.
    
    Borrows heavily from the argparse documentation examples:
    <http://docs.python.org/library/argparse.html>
    """
    
    # Construct the parser (which is stored in parser)
    # Module docstring lives in __doc__
    # See http://python-forum.com/pythonforum/viewtopic.php?f=3&t=36847
    # And a formatter class so our examples in the docstring look good. Isn't it
    # convenient how we already wrapped it to 80 characters?
    # See http://docs.python.org/library/argparse.html#formatter-class
    parser = argparse.ArgumentParser(description=__doc__, 
        formatter_class=argparse.RawDescriptionHelpFormatter)
    
    # General options
    parser.add_argument("array_files", nargs="+",
        help="CEL-format microarray files")
        
    # Logging options
    parser.add_argument("--loglevel", default="DEBUG", choices=["DEBUG", "INFO",
        "WARNING", "ERROR", "CRITICAL"],
        help="logging level to use")
        
    # The command line arguments start with the program name, which we don't
    # want to treat as an argument for argparse. So we remove it.
    args = args[1:]
        
    return parser.parse_args(args)

def set_loglevel(loglevel):
    """
    Given a string log level name, set the logging module's log level to that
    level. Raise an exception if the log level is invalid.
    
    """
    
    # Borrows heavily from (actually is a straight copy of) the recipe at
    # <http://docs.python.org/2/howto/logging.html>
    
    # What's the log level number from the module (or None if not found)?
    numeric_level = getattr(logging, loglevel.upper(), None)
    
    if not isinstance(numeric_level, int):
        # Turns out not to be a valid log level
        raise ValueError("Invalid log level: {}".format(loglevel))
    
    # Set the log level to the correct numeric value
    logging.basicConfig(format="%(levelname)s:%(message)s", level=numeric_level)

def main(args):
    """
    Parses command line arguments and do the work of the program.
    "args" specifies the program arguments, with args[0] being the executable
    name. The return value should be used as the program's exit code.
    """
    
    options = parse_args(args) # This holds the nicely-parsed options object

    # Set up logging
    set_loglevel(options.loglevel)

    # This holds running totals by probe name
    totals = collections.defaultdict(float)
    
    # This holds running second moment totals (sum of squares of data items) by
    # probe name
    square_totals = collections.defaultdict(float)
    
    for array_file in options.array_files:
        # Extract the CEL with the R script
        
        # Where should we put the extracted <name>\t<intensity> TSV?
        tsv_directory = tempfile.mkdtemp()
        tsv_file = os.path.join(tsv_directory, "extracted.tsv")
        
        logging.info("Extracting {} to {}".format(array_file, tsv_file))
        subprocess.check_call(["./extract_expression.R", array_file, tsv_file])
        
        
        logging.info("Reading {}".format(tsv_file))
        for probe, intensity in tsv.TsvReader(open(tsv_file, "r")):
            # Parse out the float intensity
            intensity = float(intensity)
            
            logging.debug("{} has intensity {}".format(probe, intensity))
            
            # Add in to totals and square_totals
            totals[probe] += intensity
            square_totals[probe] += intensity ** 2
        
        
        # Remove temporary files
        os.remove(tsv_file)
        os.rmdir(tsv_directory)
                
    for probe_name, total in totals.iteritems():
        # Calculate the average
        average = total / len(options.array_files)
        
        # Calculate the second moment
        second_moment = square_totals[probe_name] / len(options.array_files)
    
        # Calculate the variance
        variance = second_moment - average ** 2
    
        if variance > 0:
            # Calculate the standard deviation
            standard_deviation = math.sqrt(variance)
        else:
            # We can't have a standard deviation
            standard_deviation = float("NaN")
    
        # Print the average and standard deviation for each probe
        print "{}\t{}\t{}".format(probe_name, average, standard_deviation)   
    
    

if __name__ == "__main__" :
    sys.exit(main(sys.argv))
