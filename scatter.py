#!/usr/bin/env python2.7
"""
scatter: plot a scatterplot of a file of numbers. Numbers should be floats, two 
per line.

Re-uses sample code and documentation from 
<http://users.soe.ucsc.edu/~karplus/bme205/f12/Scaffold.html>
"""

import argparse, sys, os, itertools, math
import matplotlib
from matplotlib import pyplot

def parse_args(args):
    """
    Takes in the command-line arguments list (args), and returns a nice argparse
    result with fields for all the options.
    Borrows heavily from the argparse documentation examples:
    <http://docs.python.org/library/argparse.html>
    """
    
    # The command line arguments start with the program name, which we don't
    # want to treat as an argument for argparse. So we remove it.
    args = args[1:]
    
    # Construct the parser (which is stored in parser)
    # Module docstring lives in __doc__
    # See http://python-forum.com/pythonforum/viewtopic.php?f=3&t=36847
    # And a formatter class so our examples in the docstring look good. Isn't it
    # convenient how we already wrapped it to 80 characters?
    # See http://docs.python.org/library/argparse.html#formatter-class
    parser = argparse.ArgumentParser(description=__doc__, 
        formatter_class=argparse.RawDescriptionHelpFormatter)
    
    # Now add all the options to it
    parser.add_argument("data", type=argparse.FileType('r'),
        help="the file to read")
    parser.add_argument("--title", default="Scatterplot",
        help="the plot title")
    parser.add_argument("--x_label", default="X",
        help="the plot title")
    parser.add_argument("--y_label", default="Y",
        help="the plot title")
    parser.add_argument("--font_size", type=int, default=12,
        help="the font size for text")
        
    return parser.parse_args(args)
    

def main(args):
    """
    Parses command line arguments, and plots a histogram.
    "args" specifies the program arguments, with args[0] being the executable
    name. The return value should be used as the program's exit code.
    """
    
    options = parse_args(args) # This holds the nicely-parsed options object
    
    # Thsi holds the X coordinates for each data point
    x_data = []
    # And this holds the Y coordinates
    y_data = []
    
    for line in options.data:
        # Unpack and parse the two numbers on this line
        parts = map(float, line.split())
        
        # Put each in the appropriate list.
        x_data.append(parts[0])
        y_data.append(parts[1])
    
    # Do the plot 
    pyplot.scatter(x_data, y_data)
    # StackOverflow provides us with font sizing
    # http://stackoverflow.com/questions/3899980/how-to-change-the-font-size-on-a-matplotlib-plot
    matplotlib.rcParams.update({"font.size": options.font_size})
    pyplot.title("{} (n = {})".format(options.title, len(x_data)))
    pyplot.xlabel(options.x_label)
    pyplot.ylabel(options.y_label)
    
    pyplot.show()
        
    return 0

if __name__ == "__main__" :
    sys.exit(main(sys.argv))
