#!/usr/bin/env python2.7
"""
findCopyNumber.py: Find copy number given a bunch of probe expression levels.

Re-uses sample code and documentation from 
<http://users.soe.ucsc.edu/~karplus/bme205/f12/Scaffold.html>
"""


import argparse, logging, sys, os, collections, math, random, itertools
import warnings, traceback, multiprocessing

import tsv
import pulp
import numpy, scipy.stats, scipy.optimize

import datawrangler

with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    # Get SearchIO, which is in BioPython 1.61+, and raises a warning about it
    # being experimental, which we ignore. See
    # <http://docs.python.org/2/library/warnings.html>.
    from Bio import SearchIO, SeqIO

# Constants
# Interval at which to report progress for aliasing model generation
PROGRESS_INTERVAL = 100

class PenaltyTree(object):
    """
    Maintains a tree of penalty terms, so that we can have arbitrarily many
    penalty terms without arbitrarily large constraint expressions.
    
    """
    
    def __init__(self, degree=100):
        """
        Make a new PenaltyTree. degree specifies the maximum number of terms to
        sum together at once. Only one PenaltyTree may be used on a given LP
        problem.
        
        """
        
        # This holds the number of children per node/number of terms to sum at
        # once.
        self.degree = degree
        
        # This holds all our leaf-level terms.
        self.terms = []
        
    def get_variable(self):
        """
        Return a fresh LpVariable with a unique name.
        
        """
        
        # Make the variable
        var = pulp.LpVariable("PenaltyTree_{}".format(get_id()))
        
        # Give the fresh variable to the caller.
        return var
        
    def add_term(self, term):
        """
        Add the given LP expression as a term in the tree.
        
        """
        
        self.terms.append(term)
        
    def set_objective(self, problem):
        """
        Add the sum of all terms as the given LP problem's objective. The
        PenaltyTree must have at least one term.
        
        """
        
        # Algorithm: Go through our leaves, making a variable for the sum of
        # each group of self.degree terms. Then repeat on the sums, making sums
        # of sums, and so on. When we only have one sum, make that the
        # objective.
        
        # This holds the list we're collecting
        collecting = self.terms
        
        # This holds the list of sum variables
        sums = []
        
        while len(collecting) > 1:
            logging.debug("Collecting {} terms in groups of {}".format(len(
                collecting), self.degree))
            
            for i in xrange(0, len(collecting), self.degree):
                # This holds the terms we have collected for this sum
                collected = []
                for j in xrange(0, min(self.degree, len(collecting) - i)):
                    # Grab each term up to our degree that actually exists
                    collected.append(collecting[i + j])
                    
                # This holds the variable we use to represent the sum of the
                # things we have collected
                sum_var = self.get_variable()

                # Constrain this variable to equal the sum of all the collected
                # terms
                problem += sum(collected) == sum_var

                # Add this variable for the next level of collection
                sums.append(sum_var)
               
            # Move up a level in the tree
            collecting = sums
            sums = []
           
        # We have now collected everything down to one term, which is in the
        # collecting list. Use it as our objective function.
        problem += collecting[0]
        
class SequenceGraphLpProblem(object):
    """
    Represents an LP copy number problem. You can attach several models to them,
    constrain them together, and solve the problem.
    
    Internally, contains a pulp LpProblem, and a PenaltyTree.
    
    """
    
    def __init__(self):
        """
        Make a new SequenceGraphLpProblem that we can solve.
        
        """
        
        # We need an actual LpProblem
        self.problem = pulp.LpProblem("copynumber", pulp.LpMinimize)
        
        # We also need a PenaltyTree for organizing penalty terms
        self.penalties = PenaltyTree()
        
    def constrain_approximately_equal(self, var_a, var_b, penalty=1):
        """
        Constrain the two LP variables (or constants) var_a and var_b to be
        approximately equal, subject to the given penalty.
        
        Adds the appropriate constraints to the CopyNumberLpProblem's internal
        LpProblem, and penalties to the model's PenaltyTree.
        
        """
        
        # Make an LP variable for the amount that var_b is above var_a. Note
        # that str() on variables produces their names. Also, we have to make
        # sure that this starts with a letter.
        amount_over = pulp.LpVariable("over_{}".format(get_id()), 0)
            
        # Add the constraint for not being more than that much over
        self.add_constraint(var_b <= var_a + amount_over)
        
        # Make an LP variable for the amount that var_b is below var_a
        amount_under = pulp.LpVariable("under_{}".format(get_id()), 0)
            
        # Add the constraint for not being more than that much under
        self.add_constraint(var_b >= var_a - amount_under)
        
        # Apply an equal penalty in each direction
        self.add_penalty((penalty * amount_over) + (penalty * amount_under))
            
    def add_penalty(self, term):
        """
        Add the given penalty term to the problem's objective.
        
        """
        
        # Just put the term in the PenaltyTree
        self.penalties.add_term(term)
        
    def add_constraint(self, constraint):
        """
        Add the given (exact) constraint to the problem. For approximate
        constraints, use constrain_approximately_equal() instead.
        
        """
        
        # Just add the constraint to the internal problem.
        self.problem += constraint
        
    def solve(self, save=None):
        """
        Solve the LP problem with the best solver we can find. After solving,
        you can get_ploidy() on all the AlleleGroups in Models attached to this
        problem.
        
        If save is specified, it is a filename to which to save the LP problem
        in LP format.
        
        You may only solve a SequenceGraphLpProblem once.
        
        """
        
        # Set up the penalties described by the penalty tree
        logging.info("Setting up penalties")
        self.penalties.set_objective(self.problem)
        
        if save is not None:
            logging.info("Saving problem to {}".format(save))
            self.problem.writeLP(save)
        
        logging.info("Looking for solver")
        # This is a list of solvers to use in reverse order of priority (last is
        # best)
        candidates = [
            pulp.GLPK(),
            # We're not actually using 132 threads here, just 32; the 100 means
            # use deterministic parallelism, working around the segfault issue
            # described here: 
            # <http://list.coin-or.org/pipermail/cbc/2013-March/001044.html>
            pulp.solvers.COIN_CMD(threads=132, msg=1)
        ]
        
        # This is the solver we actually use
        solver = None
        for candidate in candidates:
            if candidate.available():
                logging.info("{} is available".format(
                    candidate.__class__.__name__))
                solver = candidate
            else:
                logging.info("{} is unavailable".format(
                    candidate.__class__.__name__))
        
        if solver is None:
            logging.critical("No solver found!")
            raise Exception("No LP solver available")
        
        logging.info("Solving with {}...".format(solver.__class__.__name__))
        
        # Solve the problem
        status = self.problem.solve(solver)
        logging.info("Solution status: {}".format(pulp.LpStatus[status]))
        
        if len(self.problem.variables()) < 20:
            # It's short enough to look at.
            for var in self.problem.variables():
                logging.debug("\t{} = {}".format(var.name, pulp.value(var)))
                
        # Report the total penalty we got when we solved.
        logging.info("Penalty: {}".format(pulp.value(self.problem.objective)))
        
        if status != pulp.constants.LpStatusOptimal:
            raise Exception("Unable to solve problem optimally.")
        

class MeasurementLocation(object):
    """
    Represents a location where copy number has been (ambiguously) measured.
    Keeps track of a string measurement identifier, the mapping location in the
    genome, and the relative contribution of this location to the measurement.
    
    """
    
    def __init__(self, measurement, contig, start, end, affinity):
        """
        Make a new MeasurementLocation, representing the mapping of the given
        probe to the region between the given start and end positions, with the
        given relative affinity.
        
        If affinity is a callable, it will be called once if the affinity of
        this MeasurementLocation is requested. Otherwise, it will
        be used as the actual affinity value.
        
        """
        
        # Save the measurement name
        self.measurement = measurement
        # And the contig
        self.contig = contig
        # And the start position
        self.start = start
        # And the end position
        self.end = end
        
        if callable(affinity):
            # We've been given a function to lazily call when someone asks for
            # our affinity. Remember it.
            self.affinity_function = affinity
            
            # Make a place to cache the affinity value we get when we call the
            # above function.
            self.affinity = None
            
        else:
            # Just use the given affinity as our actual affinity value.
            self.affinity = affinity
            
    def get_affinity(self):
        """
        Return the affinity of this MeasurementLocation: the weight with which
        the AlleleGroup it is in contributes to the total measurement.
        
        If there is no affinity stored, call the affinity function and cache its
        result. Otherwise, return the stored affinity.
        
        """
        
        if self.affinity is None:
            # Go calculate the affinity. This is expensive and probably goes and
            # calls out to R
            logging.debug("Calculating affinity for {} at {}:{}-{}".format(
                self.measurement, self.contig, self.start, self.end))
            self.affinity = self.affinity_function()
            
        return self.affinity
        
class AlleleGroup(object):
    """
    Represents an AlleleGroup. Has a ploidy, and maintains a list of
    MeasurementLocations inside it.
    
    """
    
    def __init__(self, contig, start, end, min_ploidy=0):
        """
        Make a new AlleleGroup, with an integer LP variable to represent its
        ploidy. 
        
        contig, start, and end specify this AlleleGroup's position in the linear
        reference; they must all be None for contigs that are not present in the
        reference.
        
        min_ploidy, if specified, gives a minimum ploidy for the allele
        group.
        
        """
        
        # Save our reference chromosome
        self.contig = contig
        # And reference start position (0-based)
        self.start = start
        # And reference end position (1-past-the-end)
        self.end = end
        
        # This holds the ploidy LP variable
        self.ploidy = pulp.LpVariable("AG_{}".format(get_id()), 
            min_ploidy, cat="Integer")
            
        # Keep a list of measurements that measure us
        self.measurements = []
            
            
            
    def get_ploidy(self):
        """
        Return the ploidy of this allele group as an integer. The relavent
        Linear Programing problem must be solved first.
        
        """
        
        # Go get the value from pulp
        return pulp.value(self.ploidy)
        
    def add_measurement(self, measurement_location):
        """
        Add a MeasurementLocation to this AlleleGroup's measurements list.
        
        The MeasurementLocation should be contained within the AlleleGroup in
        the genome.
        
        """
        
        # Add it to the list
        self.measurements.append(measurement_location)
        
    def get_measurements(self):
        """
        Yield each of the MeasurementLocations added to this AlleleGroup.
        
        """
        
        for measurement in self.measurements:
            yield measurement
        
class Model(object):
    """
    Represents a Sequence Graph model of the copy number of a genome. It
    contains lists of AlleleGroups with reference genome locations, separated
    out by contig. New AlleleGroups can be inserted, and, when all AlleleGroups
    have been made, approximate-equality constraints can be generated tying
    AlleleGroup copy numbers to those of their neighbors.
    
    All constraints and penalty terms are automatically added to the
    SequenceGraphLpProblem to which the Model is attached (specified on
    construction).
    
    """
    
    def __init__(self, problem):
        """
        Make a new Model attached to the given SequenceGraphLpProblem. Once the
        problem is solved, the Model will have copy number values available from
        its AlleleGroups.
        
        """
        
        # We need a place to keep our AlleleGroup lists
        self.allele_groups = collections.defaultdict(list)
        
        # We need to remember the problem we're attached to
        self.problem = problem
        
    def constrain_approximately_equal(self, var_a, var_b, penalty=1):
        """
        Constrain the two LP variables (or constants) var_a and var_b to be
        approximately equal, subject to the given penalty.
        
        Adds the appropriate constraints and penalties to the Model's
        SequenceGraphLpProblem.
        
        This method is a convenience function, so that you can add constraints
        to a Model when you have only the Model, without poking around inside
        it.
        
        """
        
        # Just forward the call on to the problem.
        self.problem.constrain_approximately_equal(var_a, var_b, 
            penalty=penalty)
            
    def add_constraint(self, constraint):
        """
        Add the given constraint to the problem this Model is attached to.
        
        This method is a convenience function, so that you can add constraints
        to a Model when you have only the Model, without poking around inside
        it.
        
        """
        
        self.problem.add_constraint(constraint)
    
    def add_penalty(self, penalty): 
        """
        Add the given penalty term to the problem this Model is attached to.
        
        This method is a convenience function, so that you can add constraints
        to a Model when you have only the Model, without poking around inside
        it.
        
        """
        
        self.problem.add_penalty(penalty)
        
    def add_allele_group(self, allele_group):
        """
        Given an AlleleGroup with its reference position defined, put it in the
        Model.
        
        Sticks it at the end of the appropriate allele group list, so the list
        may no longer be sorted.
        
        """
        
        # Put the allele group in the right list
        self.allele_groups[allele_group.contig].append(allele_group)
        
    def get_allele_groups(self):
        """
        Iterate through all AlleleGroups in the model.
        
        """
        
        for allele_group_list in self.allele_groups.itervalues():
            # For each list of AlleleGroups on a contig
            for allele_group in allele_group_list:
                # For each AlleleGroup on that contig, yield it.
                yield allele_group
        
        
        
    def sort_allele_groups(self):
        """
        Sort all the Model's allele group lists into genome coordinate order.
        
        """
        
        for allele_group_list in self.allele_groups.itervalues():
            # Sort each list in place, by start coordinate and then end
            # coordinate
            allele_group_list.sort(key=lambda group: (group.start, group.end))
            
    def build_aliasing_model(self):
        """
        Takes a Model and adds measurement aliasing LP variables to it.
        
        Returns a dict of LP variables representing total hybridization (where
        1.0 = 1 perfect match copy) by probe name.
        
        Assumes we're using microarray data, where MeasurementLocations are
        probe mappings.
        
        """
        
        # This dict holds probe hybridization expressions by probe name
        hybridization_expressions = collections.defaultdict(list) 
        
        logging.info("Scanning for mapping locations")
        
        for allele_group in self.get_allele_groups():
            for measurement in allele_group.measurements:
                # Work out this measurement location's hybridization
                # contribution expression
                contribution = (measurement.get_affinity() *
                    allele_group.ploidy)
                
                # Add it in to the appropriate probe hybridization expression
                hybridization_expressions[measurement.measurement] = \
                    hybridization_expressions[measurement.measurement] + \
                    contribution
            
        # This holds hybridization LP variables by probe name
        hybridizations = {}
        
        logging.info("Creating hybridization variables")
                
        for probe, hybridization_expression in \
            hybridization_expressions.iteritems():
            
            # Make an LP variable for each of these hybridization expressions
            probe_hybridization = pulp.LpVariable(
                "{}_hybridization_{}".format(probe, get_id()), 0)
                
            # Set it equal to its expression
            self.add_constraint(hybridization_expression == 
                probe_hybridization)
            
            # Add the LP variable to the dict we're building
            hybridizations[probe] = probe_hybridization
        
        # Give back the completed dict of probe hybridization LP variables
        return hybridizations
            
    def add_constraints(self, genome_length, dna_penalty=0, end_penalty=1E9,
        default_ploidies=collections.defaultdict(lambda: 2),
        breakpoint_weight=1):
        """
        Must be called after all add_allele_group() calls, and after the allele
        group lists have been sorted with sort_allele_groups(). Constrains
        adjacent allele groups together, and puts those constraints in the
        attached SequenceGraphLpProblem.
        
        This function may only be called once.
        
        genome_length is the number of potential breakpoint positions (i.e.
        bases) in the genome.
        
        dna_penalty is how much to charge per base of gained/lost DNA.
        
        end_penalty is the penalty to charge per duplicated/deleted AlleleGroup
        at the end of a contig.
        
        default_ploidies is a dict specifying a prior copy number for each
        AlleleGroup; deviations from this copy number are penalized.
        
        breakpoint_weight is a factor by which to multiply the distance-
        determined breakpoint penalty.
        
        """
        
        for allele_group_list in self.allele_groups.itervalues():
            # This holds the previous allele group we visited
            last_group = None
            
            for group in allele_group_list:
                if last_group is None:
                    # This is the first group. Constrain it to the default copy
                    # number (i.e. the assumed number of telomeres).
                    
                    self.constrain_approximately_equal(group.ploidy, 
                        default_ploidies[group], penalty=end_penalty)
                    
                else:
                
                    # What's the end-end distance from the last thing? Since the
                    # end location is really 1-past-the-end, we need to subtract
                    # 1 from it to get the last base actually included in the
                    # previous region.
                    distance = group.start - (last_group.end - 1)
                
                    if distance <= 0:
                        # There is at least 1 base of overlap. They must be
                        # equal.
                        self.add_constraint(group.ploidy == last_group.ploidy)
                    else:
                        # There is no overlap. Allow them to vary.
                        
                        # We want to charge for the gained/lost DNA.
                        
                        # How much do we need to charge for a copy number change
                        # on either side of this stretch of DNA? Assume we have
                        # to charge for half the bases between the centers.
                        penalty = float(group.start + group.end - 
                            last_group.start - last_group.end) / 2 * dna_penalty
                        
                        # If the previous AlleleGroup changes copy number from
                        # its default, charge it for half the DNA between it and
                        # here.
                        self.constrain_approximately_equal(last_group.ploidy, 
                            default_ploidies[last_group], penalty=penalty)
                        # Charge us for the other half
                        self.constrain_approximately_equal(group.ploidy, 
                            default_ploidies[group], penalty=penalty)
                        
                        # The copy number change penalty is the negative log
                        # likelihood of a break between them, which is the
                        # negative log of the probability that a breakpoint
                        # would hit here rather than anywhere else in the
                        # genome. We're really just adding/subtracting a
                        # constant to the breakpoint_penalty, so we take it from
                        # the user so that our breakpoint penalty is consistent
                        # for different probe sets. TODO: We don't properly
                        # account for longer genomes picking up more
                        # breakpoints.
                        breakpoint_penalty = -math.log(float(distance) / 
                            genome_length) * breakpoint_weight
                        
                        # Add the breakpoint constriant: charge the breakpoint
                        # penalty for any copy number difference between the
                        # previous allele group and this one.
                        self.constrain_approximately_equal(last_group.ploidy, 
                            group.ploidy, penalty=breakpoint_penalty)
                
                # Now we're done with this group, so it's the last
                last_group = group
                
            if len(allele_group_list) > 1:
                # Constrain the last allele group in a contig (if it isn't also
                # the first) to its default ploidy, so it's not unrealistically
                # cheap to duplicate or delete ends.
                self.constrain_approximately_equal(last_group.ploidy, 
                    default_ploidies[last_group], penalty=end_penalty)
                    
    def save_map(self, stream, measured_ratios=None, 
        reference_hybridizations=None):
        """
        Save this Model to the given stream as a human-readable copy number map.
        
        The SequenceGraphLpProblem that the Model is attached to must be solved.
        
        measured_ratios is an optional dict of the sample/normal ratios for each
        MeasurementLocation measurement name (i.e. probe name).
        
        reference_hybridizations is an optional dict of the total effective
        perfect match copy number of the reference genome for each probe.
        
        """
        
        for contig, allele_groups in self.allele_groups.iteritems():
            # Print the normal copy number for each allele group
            stream.write("{}:\n".format(contig))
            for allele_group in allele_groups:
                
                # Print the copy number for the region. These may disagree where
                # probes overlap.
                stream.write("\t{} ({}:{}-{})".format(allele_group.get_ploidy(),
                    allele_group.contig, allele_group.start, allele_group.end))
                
                for mapping in allele_group.measurements:
                    # Put the probe that maps here    
                    stream.write(" ")
                    stream.write(mapping.measurement)
                    
                    
                    if measured_ratios is not None: 
                        #  Put the ratio
                        stream.write(" R: {}".format(measured_ratios[
                            mapping.measurement]))
                    
                    if reference_hybridizations is not None:
                        # Put the hybridization here vs. the total
                        stream.write(" A:{}/{}".format(mapping.get_affinity(),
                        reference_hybridizations[mapping.measurement]))
                    
                stream.write("\n")
                
    def save_bedgraph(self, stream, track_name="copyNumber",
        track_description=None, y_line=2, grid=False):
        """
        Save the Model's copy numbers as a BEDgraph-format file (BED file where
        the feature names are really values to plot, with an appropriate browser
        track header) written to the given stream.
        
        The SequenceGraphLpProblem that the Model is attached to must be solved.
        
        track_name is an optional name for the track in the browser.
        
        track_description is an optional description for the track, also
        displayed in the browser.
        
        y_line is a number at which a horizontal line can be drawn. The default
        is 2, and None disables the line.
        
        If grid is true, display a line at y=0.
        
        Automatically quotes name and description.
        
        """
        
        # If there's no description, use the track's name
        if track_description is None:
            track_description = track_name
        
        # Find the maximum copy number used, or use 2 if it's smaller than that.
        max_copy_number = 2
        
        for allele_groups in self.allele_groups.itervalues():
            for allele_group in allele_groups:
                # Scan through all the allele groups looking for the highest
                # ploidy we called anywhere.
                if allele_group.get_ploidy() > max_copy_number:
                    # This is the new highest copy number observed.
                    max_copy_number = allele_group.get_ploidy()
        
        # Save the copy number map as a BEDgraph.
        # Start with the appropriate header.
        stream.write("track type=bedGraph name=\"{}\" description=\"{}\" "
            "viewLimits=0:{} maxHeightPixels=50:50:50 autoScale=off".format(
            track_name, track_description, max_copy_number))
            
        if y_line is not None:
            # We should put a line in in the header
            stream.write(" yLineMark={} yLineOnOff=on".format(y_line))
        
        if grid:
            # Also turn on the y=0 line in the header
            stream.write(" gridDefault=on")
            
        stream.write("\n")
        
        for contig, allele_groups in self.allele_groups.iteritems():
            for group in allele_groups:
                
                # Say that the copy number under the group is what we've
                # guessed.
                stream.write("{}\t{}\t{}\t{}\n".format(contig, 
                    group.start, group.end, group.get_ploidy()))
        
def get_id():
    """
    Return a unique integer ID (for this execution). Use this to ensure your LP
    variable names are unique.
    
    """
    
    if not hasattr(get_id, "last_id"):
        # Start our IDs at 0, which means the previous ID was -1. Static
        # variables in Python are hard.
        setattr(get_id, "last_id", -1)
        
    # Advance the ID and return the fresh one.
    get_id.last_id += 1
    return get_id.last_id

    

def parse_args(args):
    """
    Takes in the command-line arguments list (args), and returns a nice argparse
    result with fields for all the options.
    
    Borrows heavily from the argparse documentation examples:
    <http://docs.python.org/library/argparse.html>
    """
    
    # Construct the parser (which is stored in parser)
    # Module docstring lives in __doc__
    # See http://python-forum.com/pythonforum/viewtopic.php?f=3&t=36847
    # And a formatter class so our examples in the docstring look good. Isn't it
    # convenient how we already wrapped it to 80 characters?
    # See http://docs.python.org/library/argparse.html#formatter-class
    parser = argparse.ArgumentParser(description=__doc__, 
        formatter_class=argparse.RawDescriptionHelpFormatter)
    
    # Make a subparsers object, so we can have both CGH- and CNV-specific
    # algorithms in the same script.
    subparsers = parser.add_subparsers(dest="mode")
    
    # Define a parent parser for shared options (see
    # http://stackoverflow.com/a/7498853/402891)
    parent = argparse.ArgumentParser(add_help=False)
    
    # General options to parent parser
    parent.add_argument("database_root",
        help="location of sample database directory tree")
    parent.add_argument("--probe_penalty", type=float, default=10,
        help="approximate equality penalty term for sample/normal ratio")
    parent.add_argument("--sample_dna_penalty", type=float, default=1E-6,
        help="penalty to charge per base of added/lost sample DNA")
    parent.add_argument("--normal_dna_penalty", type=float, default=1,
        help="penalty to charge per base of added/lost normal DNA")
    parent.add_argument("--genome_length", type=float, default=3E12,
        help="length of the genome in base pairs")
    parent.add_argument("--breakpoint_weight", type=float, default=1.0,
        help="penalty weight for breakpoint penalties")
    parent.add_argument("--scale", type=float, default=1.0,
        help="amount by which to scale sample/normal ratio deviations from 1")
        
    # Logging options
    parent.add_argument("--loglevel", default="DEBUG", choices=["DEBUG", "INFO",
        "WARNING", "ERROR", "CRITICAL"],
        help="logging level to use")
    
    # This is for Comparative Genomic Hybridization-specific options (when we
    # have sample/normal log ratios fro a bunch of probes).
    cgh_parser = subparsers.add_parser("cgh", parents=[parent],
        help="process Comparative Genomic Hybridization data")
    
    # CGH options
    cgh_parser.add_argument("sample",
        help="ID of sample to process from the database")
    cgh_parser.add_argument("--stability_region", nargs=3,
        help="contig, start, and end of a region to check stability in")
    cgh_parser.add_argument("--stability_bedgraph", type=argparse.FileType("w"),
        default=None,
        help="file to save stability deleted-probe BEDgraph in")
    cgh_parser.add_argument("--save_problem", type=str, default=None,
        help="file to save the LP problem to in CPLEX format")
    cgh_parser.add_argument("--save_maps", type=argparse.FileType("w"), 
        default=sys.stdout,
        help="file to save copy number maps in")
    cgh_parser.add_argument("--save_bedgraph", type=argparse.FileType("w"),
        default=None,
        help="file to save copy number map as a BEDgraph in")
    cgh_parser.add_argument("--raw_bedgraph", type=argparse.FileType("w"), 
        default=None,
        help="file to save a BEDgraph of raw ratios to")
    cgh_parser.add_argument("--weighted_bedgraph", type=argparse.FileType("w"), 
        default=None,
        help="file to save a BEDgraph of weighted ratios to")
    cgh_parser.add_argument("--bedgraph_name", type=str, default="copyNumber",
        help="track name for the BEDgraph to be saved")

    # This is for working out probe-specific weights for CGH data
    cgh_weights_parser = subparsers.add_parser("cgh-weights", parents=[parent],
        help="produce weights for Comparative Genomic Hybridization data")
        
    # CGH weight calculation options
    cgh_weights_parser.add_argument("array",
        help="array name to generate weights from controls in database for")
        
    # This is for doing a whole batch of CGH samples
    cgh_all_parser = subparsers.add_parser("cgh-all", parents=[parent],
        help="process all Comparative Genomic Hybridization data for an array")
    
    # CGH all-samples options
    cgh_all_parser.add_argument("array",
        help="array name to process CGH data for")
    cgh_all_parser.add_argument("--out_dir",
        help="path to create output files under")
    cgh_all_parser.add_argument("--stability_region", nargs=3,
        help="contig, start, and end of a region to check stability in")
        
    # This is for single CNV probes that aren't supposed to cover SNPs, but for
    # which we have only intensity values.
    cnv_parser = subparsers.add_parser("cnv", parents=[parent],
        help="process CNV unpaired probe intensities")
        
    # CNV options
    cnv_parser.add_argument("sample",
        help="ID of sample to process from the database")
    cnv_parser.add_argument("--save_problem", type=str, default=None,
        help="file to save the LP problem to in CPLEX format")
    cnv_parser.add_argument("--save_maps", type=argparse.FileType("w"), 
        default=sys.stdout,
        help="file to save copy number maps in")
    cnv_parser.add_argument("--save_bedgraph", type=argparse.FileType("w"),
        default=None,
        help="file to save copy number map as a BEDgraph in")
    cnv_parser.add_argument("--raw_bedgraph", type=argparse.FileType("w"), 
        default=None,
        help="file to save a BEDgraph of raw ratios to")
    cnv_parser.add_argument("--weighted_bedgraph", type=argparse.FileType("w"), 
        default=None,
        help="file to save a BEDgraph of weighted ratios to")
    cnv_parser.add_argument("--ambiguity_bedgraph", type=argparse.FileType("w"), 
        default=None,
        help="file to save a BEDgraph of total probe affinity to")
    cnv_parser.add_argument("--bedgraph_name", type=str, default="copyNumber",
        help="track name for the BEDgraph to be saved")
        
    # This is for paired CNV probe samples: a "sample" that contains some tumor
    # cells, and a "background" which doesn't.
    cnv_paired_parser = subparsers.add_parser("cnv-paired", parents=[parent],
        help="process CNV paired-sample data")
        
    # Paired CNV options
    cnv_paired_parser.add_argument("sample",
        help="ID of partially-pure tumor sample to process from the database")
    cnv_paired_parser.add_argument("background",
        help="ID of pure non-tumor sample")
    cnv_paired_parser.add_argument("purity", type=float,
        help="purity of the tumor in the sample (0 to 1)")
    cnv_paired_parser.add_argument("--save_problem", type=str, default=None,
        help="file to save the LP problem to in CPLEX format")
    cnv_paired_parser.add_argument("--save_maps", type=argparse.FileType("w"), 
        default=sys.stdout,
        help="file to save copy number maps in")
    cnv_paired_parser.add_argument("--save_bedgraph", 
        type=argparse.FileType("w"), default=None,
        help="file to save copy number map as a BEDgraph in")
    cnv_paired_parser.add_argument("--raw_bedgraph", 
        type=argparse.FileType("w"), default=None,
        help="file to save a BEDgraph of raw ratios to")
    cnv_paired_parser.add_argument("--weighted_bedgraph", 
        type=argparse.FileType("w"), default=None,
        help="file to save a BEDgraph of weighted ratios to")
    cnv_paired_parser.add_argument("--ambiguity_bedgraph", 
        type=argparse.FileType("w"), default=None,
        help="file to save a BEDgraph of total probe affinity to")
    cnv_paired_parser.add_argument("--bedgraph_name", type=str, 
        default="copyNumber",
        help="track name for the BEDgraph to be saved")
        
    # This is for working out probe-specific weights for CNV data
    cnv_weights_parser = subparsers.add_parser("cnv-weights", parents=[parent],
        help="produce weights for Affymetrix CNV probe data")
        
    # CNV weight calculation options
    cnv_weights_parser.add_argument("array",
        help="array name to generate weights from controls in database for")
    
    # This is for doing a whole batch of CNV samples
    cnv_all_parser = subparsers.add_parser("cnv-all", parents=[parent],
        help="process all Copy Number Variation probe data for an array")
        
    # CNV all-samples options
    cnv_all_parser.add_argument("array",
        help="array name to process CGH data for")
    cnv_all_parser.add_argument("--out_dir",
        help="path to create output files under")
        
    # This is for pairs of CNV samples
    cnv_pair_parser = subparsers.add_parser("cnv-pair", parents=[parent],
        help="process Copy Number Variation probe data for paired samples")
        
    # CNV paired-samples options
    cnv_pair_parser.add_argument("normal",
        help="ID of normal sample to process from the database")
    cnv_pair_parser.add_argument("tumor",
        help="ID of tumor sample to process from the database")      
    cnv_pair_parser.add_argument("tumor_purity", type=float, default=1.0,
        help="portion of the tumor sample that is actual tumor cells")  
        
    # This is for pre-binned, unambiguous regional copy number ratio data.
    regions_parser = subparsers.add_parser("regions", parents=[parent],
        help="process raw copy number estimates for a set of regions")
    
    # Regions Options
    regions_parser.add_argument("region_ratios", type=argparse.FileType("r"),
        help="file of <chrom>\\t<start>\\t<end>\\t<ratio> lines, in order")
    regions_parser.add_argument("--unlog", action="store_true",
        help="ratios file is really log ratios; un-log them")
    regions_parser.add_argument("--log_base", type=float, default=10.0,
        help="base of log for un-logging log ratios")
    regions_parser.add_argument("--save_problem", type=str, default=None,
        help="file to save the LP problem to in CPLEX format")
    regions_parser.add_argument("--save_maps", type=argparse.FileType("w"), 
        default=sys.stdout,
        help="file to save copy number maps in")
    regions_parser.add_argument("--save_bedgraph", type=argparse.FileType("w"),
        default=None,
        help="file to save copy number map as a BEDgraph in")
    regions_parser.add_argument("--raw_bedgraph", type=argparse.FileType("w"), 
        default=None,
        help="file to save a BEDgraph of raw ratios to")
    regions_parser.add_argument("--weighted_bedgraph", 
        type=argparse.FileType("w"), default=None,
        help="file to save a BEDgraph of weighted ratios to")
    regions_parser.add_argument("--bedgraph_name", type=str, 
        default="copyNumber",
        help="track name for the BEDgraph to be saved")
    
    # The command line arguments start with the program name, which we don't
    # want to treat as an argument for argparse. So we remove it.
    args = args[1:]
        
    return parser.parse_args(args)

def set_loglevel(loglevel):
    """
    Given a string log level name, set the logging module's log level to that
    level. Raise an exception if the log level is invalid.
    
    """
    
    # Borrows heavily from (actually is a straight copy of) the recipe at
    # <http://docs.python.org/2/howto/logging.html>
    
    # What's the log level number from the module (or None if not found)?
    numeric_level = getattr(logging, loglevel.upper(), None)
    
    if not isinstance(numeric_level, int):
        # Turns out not to be a valid log level
        raise ValueError("Invalid log level: {}".format(loglevel))
    
    # Set the log level to the correct numeric value
    logging.basicConfig(format="%(levelname)s:%(message)s", level=numeric_level)

def get_contig_maps(affinities_file, probes_available=None, probe_list=None):
    """
    Given a stream of affinities (<probe
    name>\t<contig>\t<start>\t<end>\t<affinity>), produce "contig maps".
    
    Returns a dict of contig maps by contig name. Each contig map is a list of
    MeasurementLocation objects for probe mapping locations, in order of their
    appearance along each chromosome (i.e. by start and then end).
    
    If probes_available is specified, it must be a set of probes for which
    measurements have been taken; probes not in that set will be ignored.
    
    If probe_list is specified, it must be a file name. If the file does not
    exist, all the probe mapping locations will be read, and the IDs of probes
    that pass the BED filter will be written to the probe_list file, one per
    line. If the file does exist, probe IDs will be read from it, and only those
    probes will be considered.
    
    Probes in the mapping but not in the dict of probe sequences will be
    ignored.
    
    """
    
    # This holds a list of (start pos, end pos, probe name, mismatches) tuples
    # for each genome contig we mapped any probes to. We'll eventually sort all
    # the lists into start-to-end "maps" of contigs in terms of probes.
    contig_maps = collections.defaultdict(list)

    logging.info("Reading mapping location affinities")

    if probe_list is not None and os.path.exists(probe_list):
        # We have a list of probes to read already. Get a set of the probe IDs
        # to consider.
        probes_to_use = set(line.strip() for line in open(probe_list, "r"))
        logging.info("Using probe list from {}".format(probe_list))
    else:
        # No iterator: consider all the probes
        probes_to_use = None
        logging.info("Considering all probes")

    # This holds a list of all the probes we actually end up using. We will
    # write it out later if the probe_list file didn't exist.
    probes_used = []

    for parts in tsv.TsvReader(affinities_file):
        # Unpack the probe name, mapping position, and mapping affinity.
        probe_name, contig, start, end, affinity = parts
        
        # Parse out numbers
        start = int(start)
        end = int(end)
        affinity = float(affinity)

        if probes_to_use is not None and probe_name not in probes_to_use:
            # We have a list and this probe is not on it. Skip it.
            continue
            
        if probes_available is not None and probe_name not in probes_available:
            # We have a set of measured probes and this probe is not in it.
            continue

        # We're using this probe.
        probes_used.append(probe_name)
                
        # Put a MeasurementLocation in the list for this reference contig
        contig_maps[contig].append(MeasurementLocation(probe_name, contig, 
            start, end, affinity))
                
    if probe_list is not None and not os.path.exists(probe_list):
        # Write the list of probes we actually used to the probe list
        
        logging.info("Writing {} probe IDs to {}".format(len(probes_used),
            probe_list))
        
        probe_list_file = open(probe_list, "w")
        
        for probe in probes_used:
            # Put each probe ID on its own line
            probe_list_file.write("{}\n".format(probe))
            
        probe_list_file.close()
    
    # Sort the contig maps.
    for contig_map in contig_maps.itervalues():
        contig_map.sort(key=lambda mapping: (mapping.start, mapping.end))
    
    return contig_maps
    
def save_bedgraph(model, data, stream, track_name="copyNumber", y_line=1, 
    grid=False):
    """
    Given a Model with one MeasurementLocation per AlleleGroup, save a BEDgraph
    that has, for each MeasurementLocation, the value for that measurement from
    the data dict. Writes the BEDgraph to the given stream.
    
    track_name is an optional name for the track in the browser, which is
    automatically quoted.
    
    y_line specifies a height at which to draw a horizontal line across the
    whole track. The default is 1, and None turns the lien off.
    
    If grid is true, draw a line at y=0.
    
    """
    
    # Start with the appropriate header.
    stream.write("track type=bedGraph name=\"{}\" " 
        "alwaysZero=on maxHeightPixels=50:50:50".format(track_name))
        
    if y_line is not None:
        # We should put a line in in the header
        stream.write(" yLineMark={} yLineOnOff=on".format(y_line))
    
    if grid:
        # Also turn on the grid (just one line, really) in the header
        stream.write(" gridDefault=on")
        
    stream.write("\n")
    
    # TODO: don't go messing about with what ought to be private fields.
    for contig, allele_groups in model.allele_groups.iteritems():
        for allele_group in allele_groups:
            for measurement in allele_group.measurements:
                # Say that the value under this location is what we get from the
                # data dict.
                stream.write("{}\t{}\t{}\t{}\n".format(contig, 
                    measurement.start, measurement.end, 
                    data[measurement.measurement]))
                
def main(args):
    """
    Parses command line arguments and do the work of the program.
    "args" specifies the program arguments, with args[0] being the executable
    name. The return value should be used as the program's exit code.
    """
    
    options = parse_args(args) # This holds the nicely-parsed options object

    # Set the log level
    set_loglevel(options.loglevel)

    if options.mode == "cgh":
        # We have Comparative Genimic Hybridization data (sample/normal log
        # ratios) to process. Use the appropriate function, which does all the
        # rest of the work of the script.
        return run_cgh(options)
    elif options.mode == "cgh-weights":
        # We want to pre-calculate probe-specific weights from some control CGH
        # samples.
        return run_cgh_weights(options)
    elif options.mode == "cgh-all":
        # We want to run the cgh command for all the samples in the given array
        return run_cgh_all(options)
    elif options.mode == "cnv":
        # We have single CNV probe intensities (unnormalized)
        return run_cnv(options)    
    elif options.mode == "cnv-paired":
        # We have CNV probe intensities, and we have a pair of samples and a
        # purity.
        return run_cnv_paired(options)       
    elif options.mode == "cnv-weights":
        # We want to pre-calculate probe-specific weights from some control CNV
        # samples.
        return run_cnv_weights(options)  
    elif options.mode == "cnv-all":
        # We want to run the cnv command for all the samples in the given array
        return run_cnv_all(options)
    elif options.mode == "regions":
        # We have abstract sample/reference ratio data for defined regions on
        # the genome.
        return run_regions(options)
    else:
        # We can't do this mode yet.
        raise Exception("Can't process {} data".format(options.mode))

def make_cgh_model(probe_affinities, measured_ratios, genome_length=3E12, 
    sample_dna_penalty=1E-6, normal_dna_penalty=1E-4, probe_penalty=1, 
    breakpoint_weight=1):
    """
    Given a stream of a TSV probe affinities file, and a dict from probe name to
    measured ratio, return a sample Model, a normal Model, and the
    SequenceGraphLpProblem they are attached to. The problem can then be solved
    to populate the models with inferred copy numbers.
    
    genome_length specifies the number of bases in the genome, for the model of
    breakpoint probability.
    
    sample_dna_penalty specifies the negative log likelihood of the addition or
    subtraction of a single base of DNA in the sample. normal_dna_penalty
    specifies the same thing for the normal.
    
    probe_penalty specifies the negative log likelihood of not matching a
    probe's measured sample/normal ratio, per unit difference.
    
    breakpoint_weight is a weight for the breakpoint negative-log-likelihood
    penalties to scale them relative to other penalties.
    
    """
    
    # Make the LP problem
    problem = SequenceGraphLpProblem()

    # Make a model for the sample genome
    sample_model = Model(problem)
    
    # And a Model for the normal genome
    normal_model = Model(problem)
    
    # Create a map of each contig, listing all the MeasurementLocations in
    # order. Load all the probe mapping locations and affinities from the
    # affinities file. Only look at probes that we have measurements for.
    contig_maps = get_contig_maps(probe_affinities,
        probes_available=set(measured_ratios.iterkeys()))
        
    # Go through these maps and make AlleleGroups in sample and normal for each
    # MeasurementLocation
    for contig, contig_map in contig_maps.iteritems():
        for measurement in contig_map:
            # Make an AlleleGroup for the sample
            sample_group = AlleleGroup(contig, measurement.start, 
                measurement.end)
            # Attach the measurement to it, so we can pull it out later.
            sample_group.add_measurement(measurement)
            # Put it in the model
            sample_model.add_allele_group(sample_group)
            
            # Now do all that for the normal. This is the normal's corresponding
            # allele group.
            normal_group = AlleleGroup(contig, measurement.start, 
                measurement.end, min_ploidy=1)
            normal_group.add_measurement(measurement)
            normal_model.add_allele_group(normal_group)
            
    # Add the genome model constraints
    sample_model.sort_allele_groups()
    sample_model.add_constraints(genome_length, dna_penalty=sample_dna_penalty,
        breakpoint_weight=breakpoint_weight)
    normal_model.sort_allele_groups()
    normal_model.add_constraints(genome_length, dna_penalty=normal_dna_penalty,
        breakpoint_weight=breakpoint_weight)
                

    # This holds the total mapping locations to analyze
    total_hits = sum((len(contig_map) for contig_map in 
        contig_maps.itervalues()))
    
    # This holds, for each probe, the total hybridization we would see from one
    # copy of the reference used for probe mapping. We use this to discount the
    # penalty for not matching the measured ratio for probes with many mapping
    # locations, so that we regard the badness of not matching the observed
    # probe ratio as about equal no matter how many mapping locations a probe
    # has.
    reference_hybridizations = collections.defaultdict(float)
    
    for mappings in contig_maps.itervalues():
        for mapping in mappings:
            # Add every mapping location's affinity in for its probe
            reference_hybridizations[mapping.measurement] += \
                mapping.get_affinity()
    
    # Get a dict from probe name to LP variable representing total hybridization
    # in the sample.
    sample_aliasing_model = sample_model.build_aliasing_model()
    # And the same thing in the normal.
    normal_aliasing_model = normal_model.build_aliasing_model()
    
    for probe, sample_hybridization in sample_aliasing_model.iteritems():
        # Go through and make measurements from the two models agree with our
        # observed ratio.
    
        # Get the normal LP variable for this probe
        normal_hybridization = normal_aliasing_model[probe]
        
        # Make a variable to represent the expected sample hybridization, given
        # the normal hybridization and constant ratio.
        expected_sample_hybridization = pulp.LpVariable(
            "{}_expected_sample_hybridization_{}".format(probe, get_id()), 0)
            
        # Make it be equal to the normal hybridization times the constant ratio.
        # TODO: Can we simplify this out?
        problem.add_constraint(expected_sample_hybridization == 
            (normal_hybridization * measured_ratios[probe]))
        
        # Work out the mismatch penalty here
        penalty = probe_penalty
        
        if reference_hybridizations[probe] > 1:
            # Reduce the penalty for multi-mapping probes because the scale of
            # the mismatches encountered due to noise will be higher.
            penalty /= reference_hybridizations[probe]
            
        # Constrain the sample hybridization to be approximately equal to the
        # normal hybridization times the constant ratio. TODO: The penalty
        # should be such that the distribution it describes for
        # probe_sample_hybridization has the right standard deviation for the
        # standard deviation of log ratios.
        problem.constrain_approximately_equal(sample_hybridization, 
            expected_sample_hybridization, penalty=penalty)
            
    
    logging.info("Made models with {} mapping locations of {} probes.".format(
        total_hits, len(sample_aliasing_model)))
    
    return sample_model, normal_model, problem

def get_variant_endpoints(model, contig, start, end):
    """
    Given a solved Model, and a region on a contig that contains zero or one
    copy number variation locations, return the start and end locations of that
    variant (in terms of bounds of the breakpoint region), as well as a
    set of the names of probes flanking the deletion.
    
    Assumes there are one or zero variants.
    
    """
    
    # Now find the start and end positions of the variant in the region of
    # interest. Hold tuples of the bounds of the breakpoint regions.
    variant_start = None
    variant_end = None
    
    # This is the set of probes flanking the copy number changes
    flanking_probes = set()
    
    # This holds the previous allele group, since we're going through
    # breakpoints.
    last_allele_group = None
    for allele_group in model.allele_groups[contig]:
        if (last_allele_group is None or 
            last_allele_group.end > end or
            allele_group.start < start):
                
            # This breakpoint is not in the range (or the one before the
            # beginning of the contig)
            
            # Advance to the next breakpoint
            last_allele_group = allele_group
            continue
        
        if (last_allele_group.get_ploidy() == 2 and
            allele_group.get_ploidy() != 2):
            
            logging.debug("Variant start detected.")
            
            # A variation just started. Record its start.
            variant_start = (last_allele_group.end, allele_group.start)
            
            # Record the flanking probes
            flanking_probes.add(allele_group.measurements[0].measurement)
            flanking_probes.add(last_allele_group.measurements[0].measurement)
        
        elif (allele_group.get_ploidy() == 2 and
            last_allele_group.get_ploidy() != 2):
            
            logging.debug("Variant end detected.")
            
            # A variation just ended. Record its end.         
            variant_end = (last_allele_group.end, allele_group.start)
            
            # Record the flanking probes
            flanking_probes.add(allele_group.measurements[0].measurement)
            flanking_probes.add(last_allele_group.measurements[0].measurement)
                
        # Advance to the next breakpoint
        last_allele_group = allele_group
        
    return variant_start, variant_end, flanking_probes
        
def run_cgh(options):
    """
    Process Comparative Genomic Hybridization log ratios. Takes the program's
    command-line optons struct, and does all the work of the program for CGH
    data.
    
    """
    
    # Make a DataWrangler from which to obtain the sample
    wrangler = datawrangler.DataWrangler(options.database_root)
    
    if wrangler.get_technology(options.sample) != "cgh":
        # This sample is the wrong technology to do CGH processing on.
        raise Exception("Sample {} is not CGH".format(options.sample))
    
    # What array does this sample belong to?
    array = wrangler.get_array(options.sample)
    
    # What condition is this sample?
    condition = wrangler.get_condition(options.sample)
        
    # This will hold the measured probe hybridization ratios (as floats) by
    # probe name.
    raw_measured_ratios = wrangler.load_sample(options.sample)
        
    # This holds the weighted hybridization ratios as floats by sample name
    measured_ratios = wrangler.load_weighted_ratios(options.sample)
    
    # These hold the Model for the sample genome, the Model for the normal
    # genome, and the SequenceGraphLpProblem that the Models are attached to.
    sample_model, normal_model, problem = make_cgh_model(
        wrangler.open_affinities(array), measured_ratios, 
        genome_length=options.genome_length, 
        sample_dna_penalty=options.sample_dna_penalty, 
        normal_dna_penalty=options.normal_dna_penalty, 
        probe_penalty=options.probe_penalty,
        breakpoint_weight=options.breakpoint_weight)   
    
    # Solve the problem
    problem.solve(save=options.save_problem)
    
    if options.stability_region is not None:
        # We should evaluate the stability of the variation in the given region
        # (if one exists), by removing the endpoint probes from it, re-running
        # the model, and seeing if the calls change.
        
        # First, parse out the region.
        # This holds the contig it's on
        stability_contig = options.stability_region[0]
        # This holds the location of the start position of the region to look in
        stability_start = int(options.stability_region[1])
        # This holds the location of the end position of the region to look in
        stability_end = int(options.stability_region[2])
        
        logging.info("Evaluating stability in {}:{}-{}".format(stability_contig,
            stability_start, stability_end))
        
        # Get the original start and end intervals, as well as the flanking
        # probe names set
        original_start, original_end, flanking_probes = get_variant_endpoints(
            sample_model, stability_contig, stability_start, stability_end)
            
        logging.info("Original maximum extent {}-{}".format(original_start, 
            original_end))
        
        if original_start is not None and original_end is not None: 
        
            logging.debug("Removing probes {}".format(", ".join(
                flanking_probes)))
            
            # Remove the outermost probes, producing a new dict without them.
            # This will drop all appearances of the probes in question, not just
            # the ones at the ends of the variant region, but there's not really
            # a good way to work around that.
            clipped_measured_ratios = {probe: ratio for probe, ratio in 
                measured_ratios.iteritems() if probe not in flanking_probes}
            
            # Build a new pair of models and LP problem.
            clipped_sample_model, clipped_normal_model, clipped_problem = \
                make_cgh_model(wrangler.open_affinities(array), 
                clipped_measured_ratios, genome_length=options.genome_length, 
                sample_dna_penalty=options.sample_dna_penalty, 
                normal_dna_penalty=options.normal_dna_penalty, 
                probe_penalty=options.probe_penalty,
                breakpoint_weight=options.breakpoint_weight)   
            
            # Solve the new LP problem
            clipped_problem.solve()
            
            # Get the new endpoints
            new_start, new_end, _ = get_variant_endpoints(
                clipped_sample_model, stability_contig, stability_start,
                stability_end)
                
            logging.info("New maximum extent {}-{}".format(new_start, new_end))
            
            # Classify each endpoint as robust or brittle. Stable endpoints will
            # have the old copy number change interval contained within the new
            # one.
            start_stable = (original_start[0] > new_start[0] and 
                original_start[1] < new_start[1])
            end_stable = (original_end[0] > new_end[0] and 
                original_end[1] < new_end[1])
            
            logging.info("Start stable: {} End stable: {}".format(start_stable,
                end_stable))
                
            if options.stability_bedgraph is not None:
                # Save the stability-checked version, so we can see where its
                # endpoints are.
                clipped_sample_model.save_bedgraph(options.stability_bedgraph, 
                    track_name="{}-stability".format(options.sample))
        else:
            # Didn't find a variant at all
            logging.info("No variant present in {} to check for "
                "stability".format(options.sample))
            start_stable = None
            end_stable = None
        
        
    
    # Write the copy number maps to standard output, or a file if they're being
    # sent there. Pass along all the probe ratios we measured, and all the total
    # effective reference genome perfect match copy numbers we summed up.
    options.save_maps.write("Normal copy number:\n")
    normal_model.save_map(options.save_maps)
    options.save_maps.write("Sample copy number:\n")
    sample_model.save_map(options.save_maps, measured_ratios)
    
    if options.save_bedgraph is not None:
        # Save the sample model map as a BEDgraph.
        
        # What should the description be?
        if options.stability_region is not None:
            # Description needs condition and stability
            # Start assembling parts for it.
            parts = [options.sample, condition]
            if start_stable is not None:
                # We actaully could check this endpoint
                if start_stable:
                    parts.append("Start robust")
                else:
                    parts.append("Start brittle")
            if end_stable is not None:
                # We actaully could check this endpoint
                if end_stable:
                    parts.append("End robust")
                else:
                    parts.append("End brittle")
            
            # Assemble a logn description string
            description = ", ".join(parts)
        else:
            # Just mention the sample and the condition
            description = "{}, {}".format(options.sample, condition)
                
        # Save the model
        sample_model.save_bedgraph(options.save_bedgraph, 
            track_name=options.bedgraph_name, 
            track_description=description)
        
    if options.raw_bedgraph is not None:
        # Save a bedgraph of raw intensity data
        save_bedgraph(sample_model, raw_measured_ratios, 
            options.raw_bedgraph, track_name="{}-raw".format(
            options.bedgraph_name)) 
            
    if options.weighted_bedgraph is not None:
        # Save a bedgraph of weighted intensity data
        save_bedgraph(sample_model, measured_ratios, 
            options.weighted_bedgraph, track_name="{}-weighted".format(
            options.bedgraph_name)) 

    return 0

def run_cgh_weights(options):
    """
    Process several CGH TSV files from control samples (which are expected to
    have no copy number variation) and produce probe-specific weights to
    compensate for some probes consistently showing high or low sample/normal
    ratios.
    
    """
    
    # Make a DataWrangler from which to obtain the samples
    wrangler = datawrangler.DataWrangler(options.database_root)
    
    # Read the probe affinity lines into a list so we don't have to go through
    # the file on disk every time. It will look just like a file unless someone
    # tries to seek on it or something.
    logging.info("Pre-reading probe affinities...")
    probe_affinities = list(wrangler.open_affinities(options.array))
    logging.info("{} mapping locations read".format(len(probe_affinities)))
    
    # This defaultdict of floats holds the total sample/normal ratio for each
    # probe, for averaging.
    total_ratio = collections.defaultdict(float)
    
    # This Counter holds the number of controls contributing to each probe's
    # total.
    contributing_samples = collections.Counter()
    
    # This holds a list of sample IDs that are controls for the array we're
    # running on.
    samples = list(wrangler.get_samples(options.array, condition="Control"))
    
    for i, sample in enumerate(samples):
        # For each control sample for the array we're using
    
        logging.info("Processing control {} of {}".format(i + 1, len(samples)))
        
        if wrangler.get_technology(sample) != "cgh":
            # This sample is the wrong technology to do CGH processing on.
            raise Exception("Sample {} is \"{}\", not \"cgh\"".format(sample, 
                wrangler.get_technology(sample)))
    
        # This will hold the measured probe hybridization ratios (as floats) by
        # probe name.
        measured_ratios = wrangler.load_sample(options.sample)
            
        # These hold the Model for the sample genome, the Model for the normal
        # genome, and the SequenceGraphLpProblem that the Models are attached
        # to.
        sample_model, normal_model, problem = make_cgh_model(
            probe_affinities, measured_ratios, 
            genome_length=options.genome_length, 
            sample_dna_penalty=options.sample_dna_penalty, 
            normal_dna_penalty=options.normal_dna_penalty, 
            probe_penalty=options.probe_penalty,
            breakpoint_weight=options.breakpoint_weight)   
            
        # This is a set of all probes that are not involved in any duplications
        # or deletions, but are in the region we're looking at. We need to start
        # with everything and then throw out probes that we see in duplications
        # or deletions.
        probes_to_use = set()
        
        for allele_groups in sample_model.allele_groups.itervalues():
            for allele_group in allele_groups:
                for measurement in allele_group.measurements:
                    # Collect all the probes that actually appear in the model.
                    probes_to_use.add(measurement.measurement)
        
        # Solve the problem
        problem.solve(save=options.save_problem)
        
        logging.info("Sample started out with {} probes".format(len(
            probes_to_use)))
        
        for allele_groups in sample_model.allele_groups.itervalues():
            for allele_group in allele_groups:
                if allele_group.get_ploidy() == 2:
                    # This allele group was not duplicated or deleted; it won't
                    # let us exclude any probe measurements from contributing to
                    # the average.
                    continue
                
                for measurement in allele_group.measurements:
                    # The probe that hybridizes here is involved in a
                    # duplication or deletion; don't use it in the average.
                    
                    logging.debug("CN {} at probe {}".format(
                        allele_group.get_ploidy(), measurement.measurement))
                    probes_to_use.discard(measurement.measurement)
                    
        logging.info("{} probes had CN 2".format(len(
            probes_to_use)))
        
        # Now all the probes left in probes_to_use are those not involved in
        # duplications or deletions. 
        
        for probe in probes_to_use:
            # Add each probe to our average.
            total_ratio[probe] += measured_ratios[probe]
            contributing_samples[probe] += 1
            
    # Now we've gone through all the samples. Compute the averages.
    average_ratios = {probe: total / contributing_samples[probe] for probe, 
        total in total_ratio.iteritems()}
        
    # Compute the weights required to make things at the average ratio actually
    # 1.0.
    weights = {probe: 1.0 / average for probe, average in 
        average_ratios.iteritems()}
    
    logging.info("Writing {} weights".format(len(weights)))
    
    # Get the writer to the weights file to write.
    writer = tsv.TsvWriter(open(wrangler.weights_path(options.array), "w"))
        
    # Write out the weights to the file. TODO: Compute and write at the same
    # time.
    for probe, weight in weights.iteritems():
        writer.line(probe, weight, contributing_samples[probe])

def run_cgh_all(options):
    """
    Process all CGH samples for an array, making output files in a certain
    directory. For each sample, produces <sample>.bedgraph,
    <sample>.raw.bedgraph <sample>.weighted.bedgraph,
    <sample>.stability.bedgraph, and <sample>_maps.txt.
    
    """
    
    # We're going to use a nifty hack: add fields to our options struct and just
    # pass it along to run_cgh.
    
    # Make a DataWrangler from which to obtain the samples
    wrangler = datawrangler.DataWrangler(options.database_root)
    
    # This holds a list of sample IDs for the array we're running on.
    samples = list(wrangler.get_samples(options.array))
    
    for sample in samples:
        # Assign this sample as the --sample option
        options.sample = sample
        
        # Name the BEDgraph
        # TODO: Add condition
        options.bedgraph_name = sample
        
        # Don't save the problem
        options.save_problem = None
        
        # Set where to save the bedgraph
        options.save_bedgraph = open(os.path.join(options.out_dir, 
            "{}.bedgraph".format(sample)), "w")
            
        # And the raw bedgraph
        options.raw_bedgraph = open(os.path.join(options.out_dir, 
            "{}.raw.bedgraph".format(sample)), "w")
            
        # And the weighted bedgraph
        options.weighted_bedgraph = open(os.path.join(options.out_dir, 
            "{}.weighted.bedgraph".format(sample)), "w")
            
        # And the copy number maps
        options.save_maps = open(os.path.join(options.out_dir, 
            "{}_maps.txt".format(sample)), "w")
        
        if options.stability_region is not None:
            # The user also wants the BEDgraph with probes near the breakpoints
            # deleted
            options.stability_bedgraph = open(os.path.join(options.out_dir, 
                "{}.stability.bedgraph".format(sample)), "w")
            
        # Run the sample
        run_cgh(options)
        
    logging.info("Ran {} samples on array {}".format(len(samples),
        options.array))
        
    return 0

def run_cnv_all_worker(options):
    """
    Worker function for running CNV samples in Multiprocessing processes. All it
    does is open all the output files and then do run_cnv.
    
    """
    try:
        # Set where to save the bedgraph
        options.save_bedgraph = open(os.path.join(options.out_dir, 
            "{}.bedgraph".format(options.sample)), "w")
            
        # And the "raw" (unweighted) bedgraph
        options.raw_bedgraph = open(os.path.join(options.out_dir, 
            "{}.unweighted.bedgraph".format(options.sample)), "w")
            
        # And the weighted bedgraph
        options.weighted_bedgraph = open(os.path.join(options.out_dir, 
            "{}.weighted.bedgraph".format(options.sample)), "w")
        
        # And the ambiguity bedgraph
        options.ambiguity_bedgraph = open(os.path.join(options.out_dir, 
            "{}.ambiguity.bedgraph".format(options.sample)), "w")
            
        # And the copy number maps
        options.save_maps = open(os.path.join(options.out_dir, 
            "{}_maps.txt".format(options.sample)), "w")
        
        # Start up the actual single-sample script.    
        run_cnv(options)
        
        # Close up all the files
        options.raw_bedgraph.close()
        options.weighted_bedgraph.close()
        options.ambiguity_bedgraph.close()
        options.save_maps.close()
        
    except:
        # Put all exception text into an exception and raise that
        logging.error(traceback.format_exc())
        raise Exception(traceback.format_exc())
        
def run_cnv_all(options):
    """
    Process all CNV samples for an array, making output files in a certain
    directory. For each sample, produces <sample>.bedgraph,
    <sample>.raw.bedgraph, and <sample>_maps.txt.
    
    """
    
    # We're going to use a nifty hack: add fields to our options struct and just
    # pass it along to run_cnv.
    
    # Make a DataWrangler from which to obtain the samples
    wrangler = datawrangler.DataWrangler(options.database_root)
    
    # This holds a list of sample IDs for the array we're running on.
    samples = list(wrangler.get_samples(options.array))
    
    # This holds a list of async handles
    asyncs = []
    
    # This holds a list of sample, error message tuples
    failures = []
    
    # Make a pool of workers
    pool = multiprocessing.Pool()
    
    for sample in samples:
        try:
            # Assign this sample as the --sample option
            options.sample = sample
            
            # Name the BEDgraph
            options.bedgraph_name = sample
            
            # Don't save the problem
            options.save_problem = None
            
            # File names get filled in by run_cnv_all_worker.
                
            # Run the sample
            logging.info("Starting {}".format(sample))
            asyncs.append(pool.apply_async(run_cnv_all_worker, (options,)))
        except BaseException:
            # We failed for some reason. Give up on this sample
            failures.append((sample, traceback.format_exc()))
            logging.error("Failed processing {}".format(sample))
            logging.error(traceback.format_exc())
            
            # Continue on to the next sample.
            
    for async, sample in itertools.izip(asyncs, samples):
        # Wait for everyone to be done.
        try:
            logging.info("Waiting on {}".format(sample))
            async.get()
        except BaseException:
            # We failed for some reason. Give up on this sample
            failures.append((sample, traceback.format_exc()))
            logging.error("Failed processing {}".format(sample))
            logging.error(traceback.format_exc())
        
    logging.info("Ran {} samples on array {}".format(len(samples),
        options.array))
        
    if len(failures) > 0:
        # Complain about the samples that failed in one convenient place.
        logging.error("{} samples failed.".format(len(failures)))
        
        for sample, message in failures:
            logging.error("{} failed with: {}".format(sample, message))
        
    return 0
        
def run_regions(options):
    """
    Process binned ratio data for user-specified regions.
    
    Does not model variation in normal copy number; normal is assumed to be
    constant throughout. Assumes diploid normal.
    
    """
    
    # Make a problem to attach models to.
    problem = SequenceGraphLpProblem()
    
    # Make a Model of the genome we care about
    model = Model(problem)
    
    # This will hold the measured ratios by region name (contig:start-end)
    measured_ratios = {}
    
    # This holds a contig map dict for saving the raw bedgraph
    contig_maps = collections.defaultdict(list)
    
    # Read in the window ratios
    for parts in tsv.TsvReader(options.region_ratios):
        # We get contig, start, end, ratio
        
        if len(parts) < 4:
            # Skip blank lines and what have you
            continue
        
        # Grab the contig name
        contig = parts[0]
        # And the start
        start = int(parts[1])
        # And the end
        end = int(parts[2])
        # And the ratio
        ratio = float(parts[3])
        
        # Make a name for the region
        region_name = "{}:{}-{}".format(contig, start, end)
        
        if math.isnan(ratio):
            # LP can't handle NAN
            logging.warn("NAN ratio for {}".format(region_name))
            continue
        
        if options.unlog:
            # Measured ratio is really log of the actual ratio. Unlog it.
            ratio = math.pow(options.log_base, ratio)
            
        # Save the ratio.
        measured_ratios[region_name] = ratio
        
        # Make an AlleleGroup for this region
        allele_group = AlleleGroup(contig, start, end)
        
        # Make a MeasurementLocation for the region
        measurement = MeasurementLocation(region_name, contig, start, end, 1.0)
        
        # Add the MeasurementLocation to the contig map
        contig_maps[contig].append(measurement)
        
        # Add the MeasurementLocation to the AlleleGroup too.
        allele_group.add_measurement(measurement)
        
        # Put it in the Model
        model.add_allele_group(allele_group)
            
    # Sort the Model
    model.sort_allele_groups()
    
    # Make the consistency constraints
    model.add_constraints(options.genome_length, 
        dna_penalty=options.sample_dna_penalty,
        breakpoint_weight=options.breakpoint_weight,
        default_ploidies=collections.defaultdict(lambda: 2))
    
    # This holds the total measurement locations to analyze
    total_hits = len(measured_ratios)
    
    for contig, allele_groups in model.allele_groups.iteritems():
        for allele_group in allele_groups:
            for measurement_location in allele_group.measurements:
                # Constrain the copy number of each allele group to be about
                # that measured for it
                model.constrain_approximately_equal(allele_group.ploidy, 
                    2 * measured_ratios[measurement_location.measurement],
                    penalty=options.probe_penalty)
    
    # Solve the LP problem
    problem.solve(save=options.save_problem)
    
    # Write the copy number map
    model.save_map(options.save_maps, measured_ratios=measured_ratios)
    
    if options.save_bedgraph is not None:
        # Save the sample model map as a BEDgraph.
        model.save_bedgraph(options.save_bedgraph, options.bedgraph_name)   
        
    if options.raw_bedgraph is not None:
        # Save a bedgraph of raw intensity data
        save_bedgraph(model, measured_ratios, 
            options.raw_bedgraph, track_name="{}-raw".format(
            options.bedgraph_name))  

    logging.info("Finished run with {} measurement locations.".format(
        total_hits))
    
    return 0
    
def make_cnv_calls(ratios, contig_maps, genome_length, sample_dna_penalty, 
    breakpoint_weight, probe_penalty, priors=collections.defaultdict(lambda: 2),
    save=None):
    """
    Given a dict of probe sample/normal ratios, and a contig_maps structure
    derived from the array's affinities file, return a solved Model.
    
    genome_length is the length of the genome being analyzed, in bases, and is
    used to normalize the LP penalties.
    
    sample_dna_penalty is the penalty to charge per base of sample DNA that
    deviates from prior copy number. 
    
    breakpoint_weight is the relative weight to apply to penalties for having
    copy number breakpoints.
    
    probe_penalty is the penalty to charge for probes not matching their
    observed hybridization values.
    
    The optional priors holds a dict of prior copy number estimates for each
    Measurement in the contig maps; by default 2 is used for everything.
    
    If save is specified, the LP problem to be solved is saved with that
    filename.
    
    """
    
    # Make the LP problem
    problem = SequenceGraphLpProblem()

    # Make a model for the genome
    model = Model(problem)
    
    # Make a default ploidies dict that holds priors by AlleleGroup instead of
    # measurement.
    default_ploidies = {}
    
    # Go through the contig maps and make an AlleleGroup for each
    # MeasurementLocation
    for contig, contig_map in contig_maps.iteritems():
        for measurement in contig_map:
            # Make an AlleleGroup for the location.
            group = AlleleGroup(contig, measurement.start, measurement.end)
            # Attach the measurement to it, so we can pull it out later.
            group.add_measurement(measurement)
            # Put it in the model
            model.add_allele_group(group)
            
            # Work out the default ploidy for the AlleleGroup, by copying from
            # the measurment we used to make it.
            default_ploidies[group] = priors[measurement]
            
    # Add the genome model constraints. Make sure to pass our own default
    # ploidies.
    model.sort_allele_groups()
    model.add_constraints(genome_length, dna_penalty=sample_dna_penalty,
        breakpoint_weight=breakpoint_weight, default_ploidies=default_ploidies)
        
    # This holds, for each probe, the total hybridization we would see from one
    # copy of the reference used for probe mapping. We use this to discount the
    # penalty for not matching the measured ratio for probes with many mapping
    # locations, so that we regard the badness of not matching the observed
    # probe ratio as about equal no matter how many mapping locations a probe
    # has.
    reference_hybridizations = collections.defaultdict(float)
    
    for mappings in contig_maps.itervalues():
        for mapping in mappings:
            # Add every mapping location's affinity in for its probe
            reference_hybridizations[mapping.measurement] += \
                mapping.get_affinity()

    # Get a dict from probe name to LP variable representing total hybridization
    aliasing_model = model.build_aliasing_model()

    for probe, hybridization in aliasing_model.iteritems():
        # Go through and make measurement agree with model
    
        
        observed_ratio = ratios[probe]
        
        logging.debug("Observed hybridization ratio {} for {}".format(
            observed_ratio, probe))
    
        # What hybridization total (mapping locations * copy number * affinity)
        # was observed for this probe? Constrain that to the model's
        # representation of it.
        # TODO: better normalization, with saturation. Just assuming that the
        # average "normal" genome we have ratios against is CN2 everywhere. 
        # TODO: Go back to CGH style.
        problem.constrain_approximately_equal(observed_ratio * 
            reference_hybridizations[probe] * 2, hybridization, 
            penalty=probe_penalty)
    
    # Solve the problem
    problem.solve(save=save)
    
    return model
    
def run_cnv(options):
    """
    Process CNV data (raw intensities for each probe).
    
    """
    
    # Make a DataWrangler from which to obtain the sample
    wrangler = datawrangler.DataWrangler(options.database_root)
    
    # What array is this sample from?
    array = wrangler.get_array(options.sample)
    
    # Load the quantile-normalized intensity values as a dict from probe to
    # intensity. Don't apply the weights that turn them into sample/normal
    # ratios.
    unweighted_measured_intensities = wrangler.load_normalized(options.sample)
    
    # Load the weighted sample/normal ratios
    measured_ratios = wrangler.load_weighted_ratios(options.sample)
    
    # Scale deviations from 1 by the user-defined scaling factor.
    measured_ratios = {probe: 1.0 + options.scale * (ratio - 1.0) for probe, 
        ratio in measured_ratios.iteritems()}
    # Create a map of each contig, listing all the MeasurementLocations in
    # order. Load all the probe mapping locations and affinities from the
    # affinities file. Only look at probes that we have measurements for.
    contig_maps = get_contig_maps(open(wrangler.affinities_path(array)),
        probes_available=set(measured_ratios.iterkeys()))
        
    # Make and solve the CNV model
    model = make_cnv_calls(measured_ratios, contig_maps, options.genome_length,
        options.sample_dna_penalty, options.breakpoint_weight,
        options.probe_penalty, save=options.save_problem)
    
    # Build the reference hybridizations dict again. TODO: get it from
    # make_cnv_calls.
    # This holds, for each probe, the total hybridization we would see from one
    # copy of the reference used for probe mapping.
    reference_hybridizations = collections.defaultdict(float)
    
    for mappings in contig_maps.itervalues():
        for mapping in mappings:
            # Add every mapping location's affinity in for its probe
            reference_hybridizations[mapping.measurement] += \
                mapping.get_affinity()
        
    # Write the copy number map to standard output, or a file if it's being
    # sent there.
    model.save_map(options.save_maps, measured_ratios=measured_ratios,
        reference_hybridizations=reference_hybridizations)
    
    if options.save_bedgraph is not None:
        # Save the model map as a BEDgraph.
        model.save_bedgraph(options.save_bedgraph, 
            track_name=options.bedgraph_name, 
            track_description="{}, {}".format(options.sample,
            wrangler.get_condition(options.sample)))
        
    if options.weighted_bedgraph is not None:
        # Save a bedgraph of weighted intensity data.
        save_bedgraph(model, measured_ratios, 
            options.weighted_bedgraph, track_name="{}-weighted".format(
            options.bedgraph_name))

    if options.raw_bedgraph is not None:
        # Save a bedgraph of unweighted intensity data. Don't put a line since
        # it's arbitrarily scaled.
        save_bedgraph(model, unweighted_measured_intensities, 
            options.raw_bedgraph, track_name="{}-unweighted".format(
            options.bedgraph_name), y_line=None)
            
    if options.ambiguity_bedgraph is not None:
        # Save a bedgraph of reference total hybridizations (how ambiguous is
        # each probe location?).
        save_bedgraph(model, reference_hybridizations, 
            options.ambiguity_bedgraph, track_name="{}-ambig".format(
            options.bedgraph_name))
        
    logging.info("Finished run for {}.".format(options.sample))
    
    return 0
    
def run_cnv_paired(options):
    """
    Process paired CNV data (raw intensities for each probe). Uses a "sample"
    and a "background" instead of a sample. Calculates the copy numbers in a
    "tumor", which is 1 - purity of the sample, with the remainder of the sample
    intensities being contributed by the background.
    
    """
    
    # Make a DataWrangler from which to obtain the samples
    wrangler = datawrangler.DataWrangler(options.database_root)
    
    # What array are we working on?
    array = wrangler.get_array(options.sample)
    
    # Load the quantile-normalized intensity values as a dict from probe to
    # intensity. Don't apply the weights that turn them into sample/normal
    # ratios.
    unweighted_measured_intensities = wrangler.load_normalized(options.sample)
    
    # Load the weighted sample/normal ratios
    sample_ratios = wrangler.load_weighted_ratios(options.sample)
    
    # Load the weighted background/normal ratios
    background_ratios = wrangler.load_weighted_ratios(options.background)
    
    logging.info("Solving for pure tumor sample from {:.2%} pure {} and "
        "background {}".format(options.purity, options.sample, 
        options.background))
    
    # Calculate tumor ratios from the above and the purity, assuming intensities
    # are addative. We do this by saying the sample intensity (or ratio; it
    # doesn't really matter since intensity<->ratio is just a scaling for each
    # probe) is a weighted sum of that for the tumor and the background, and
    # then solving for the tumor.
    tumor_ratios = {probe: (sample_ratios[probe] - (1 - options.purity) * 
        background_ratios[probe]) / options.purity for probe in 
        sample_ratios.iterkeys()}
        
    # Scale deviations from 1 by the user-defined scaling factor.
    # TODO: Should we do this before the solve-for-tumor step instead?
    tumor_ratios = {probe: 1.0 + options.scale * (ratio - 1.0) for probe, 
        ratio in tumor_ratios.iteritems()}

    # Create a map of each contig, listing all the MeasurementLocations in
    # order. Load all the probe mapping locations and affinities from the
    # affinities file. Only look at probes that we have measurements for.
    # Assumes sample and background (and thus tumor) all have the same set of
    # probes, because we use this set of contig maps to solve the background and
    # the tumor.
    contig_maps = get_contig_maps(open(wrangler.affinities_path(array)),
        probes_available=set(tumor_ratios.iterkeys()))
    
    logging.info("Solving background sample {} for copy number".format(
        options.background))
        
    # Make and solve the CNV model for the background
    background_model = make_cnv_calls(background_ratios, contig_maps,
        options.genome_length, options.sample_dna_penalty,
        options.breakpoint_weight, options.probe_penalty, save=None)
        
    # This holds a dict of prior copy numbers for the tumor sample by
    # MeasurementLocation. The tumor gets charged for deviations from this, so
    # we start them out with our calls for the background.
    priors = {}
    for allele_group in background_model.get_allele_groups():
        for measurement in allele_group.get_measurements():
            # Use this AlleleGroup's ploidy in the background as the prior for
            # the tumor. TODO: This is just to undo the later translation from
            # MeasurementLocation to AlleleGroup, but it's important because the
            # AlleleGroups won't correspond to each other in the two models.
            # They'll just share MeasurementLocation references.
            priors[measurement] = allele_group.get_ploidy()
    
    logging.info("Solving inferred pure tumor for copy number".format(
        options.background))
        
    # Make and solve the CNV model for the tumor
    model = make_cnv_calls(tumor_ratios, contig_maps, options.genome_length,
        options.sample_dna_penalty, options.breakpoint_weight,
        options.probe_penalty, save=options.save_problem, priors=priors)
    
    # Build the reference hybridizations dict again. TODO: get it from
    # make_cnv_calls.
    # This holds, for each probe, the total hybridization we would see from one
    # copy of the reference used for probe mapping.
    reference_hybridizations = collections.defaultdict(float)
    
    for mappings in contig_maps.itervalues():
        for mapping in mappings:
            # Add every mapping location's affinity in for its probe
            reference_hybridizations[mapping.measurement] += \
                mapping.get_affinity()
        
    # Write the copy number map to standard output, or a file if it's being
    # sent there.
    model.save_map(options.save_maps, measured_ratios=tumor_ratios,
        reference_hybridizations=reference_hybridizations)
    
    if options.save_bedgraph is not None:
        # Save the model map as a BEDgraph.
        model.save_bedgraph(options.save_bedgraph, 
            track_name=options.bedgraph_name, 
            track_description="{}, {}".format(options.sample,
            wrangler.get_condition(options.sample)))
        
    if options.weighted_bedgraph is not None:
        # Save a bedgraph of weighted intensity data. In this case it's also
        # solved for tumor.
        save_bedgraph(model, tumor_ratios, 
            options.weighted_bedgraph, track_name="{}-weighted".format(
            options.bedgraph_name))

    if options.raw_bedgraph is not None:
        # Save a bedgraph of unweighted intensity data. Don't put a line since
        # it's arbitrarily scaled. In this case it's also not solved for tumor.
        save_bedgraph(model, unweighted_measured_intensities, 
            options.raw_bedgraph, track_name="{}-unweighted".format(
            options.bedgraph_name), y_line=None)
            
    if options.ambiguity_bedgraph is not None:
        # Save a bedgraph of reference total hybridizations (how ambiguous is
        # each probe location?).
        save_bedgraph(model, reference_hybridizations, 
            options.ambiguity_bedgraph, track_name="{}-ambig".format(
            options.bedgraph_name))
        
    logging.info("Finished run for sample {} and background {}.".format(
        options.sample, options.background))
    
    return 0
    
def run_cnv_weights(options):
    """
    Process several CNV TSV files from control samples (which are expected to
    have no copy number variation) and produce probe-specific weights to
    compensate for some probes consistently showing high or low hybridizations.
    
    For CNV samples, the quantile normalized intensities for each sample are
    multiplied by the probe-specific weights to yield sample-normal ratios, so
    these weights must be the reciprocal of "normal" intensity for each probe.
    
    If no probe-specific weights exist already, the weights are calculated as
    the reciprocal of the mode normalized intensity for each probe.
    
    If a set of probe-specific weights already exists, it means we can run copy
    number inferrence on each sample. In that case, we do that, and only take
    the mean of the samples where a probe is called as normal (CN=2).
    
    Modes are estimated with Gaussian kernel density estimation.
    
    """
    
    # Make a DataWrangler from which to obtain the samples
    wrangler = datawrangler.DataWrangler(options.database_root)
    
    # This holds a list of sample IDs that are controls for the array we're
    # running on.
    samples = list(wrangler.get_samples(options.array, condition="Control"))
    
    if len(samples) == 0:
        raise Exception("Can't find weights for array with no samples")
    
    if os.path.exists(wrangler.weights_path(options.array)):
        # Call copy numbers for everyone, and average the ones called as normal
        # for each probe.
        
        logging.info("Found existing weights, using calling-and-averaging "
            "method")
        
        # This defaultdict of floats holds the total hybridization for each
        # probe, for averaging.
        total_hybridization = collections.defaultdict(float)
        
        # This Counter holds the number of controls contributing to each probe's
        # total.
        contributing_samples = collections.Counter()
    
        for i, sample in enumerate(samples):
            # For each control sample for the array we're using
        
            logging.info("Processing control {} of {}".format(i + 1,
                len(samples)))
            
            if wrangler.get_technology(sample) != "cnv":
                # This sample is the wrong technology to do cnv processing on.
                raise Exception("Sample {} is \"{}\", not \"cnv\"".format(sample, 
                    wrangler.get_technology(sample)))
            
            # Load the normalized, unweighted values for this sample
            hybridizations = wrangler.load_normalized(sample)
            
            # A weights file exists already. Do copy number inferrence.
            
            # Load the weighted sample/normal ratios
            measured_ratios = wrangler.load_weighted_ratios(sample)
            
            # Scale deviations from 1 by the user-defined scaling factor.
            measured_ratios = {probe: 1.0 + options.scale * (ratio - 1.0) for 
                probe, ratio in measured_ratios.iteritems()}
                
            # Create a map of each contig, listing all the MeasurementLocations
            # in order. Load all the probe mapping locations and affinities from
            # the affinities file. Only look at probes that we have measurements
            # for. TODO: make this only once.
            contig_maps = get_contig_maps(open(wrangler.affinities_path(
                options.array)), probes_available=set(
                measured_ratios.iterkeys()))
        
            logging.info("Solving CNV problem for {} based on existing "
                "weights".format(sample))
        
            # Make and solve the CNV model
            model = make_cnv_calls(measured_ratios, contig_maps, 
                options.genome_length, options.sample_dna_penalty, 
                options.breakpoint_weight, options.probe_penalty)
                
            # Make a set of all the probes not called as having altered copy
            # number.
            normal_probes = set(measured_ratios.iterkeys())
            
            for allele_group in model.get_allele_groups():
                if allele_group.get_ploidy() != 2:
                    # This AlleleGroup has a copy number change. 
                    for measurement in allele_group.get_measurements():
                        # Discard all the probes that have Measurements on
                        # it.
                        normal_probes.discard(measurement.measurement)
                        
            for probe in normal_probes:
                # Add this probe's unweighted, normalized hybridization in to
                # the average
                total_hybridization[probe] += hybridizations[probe]
                contributing_samples[probe] += 1
            
        # Now we've gone through all the samples. Compute the averages.
        average_hybridizations = {probe: total / contributing_samples[probe] for
            probe, total in total_hybridization.iteritems()}
            
        # Compute the weights required to make things at the average
        # hybridization actually 1.0.
        weights = {probe: 1.0 / average for probe, average in 
            average_hybridizations.iteritems()}
        
            
    else:
        # No weights available. For each probe, estimate the mode and divide
        # everyone by that.
        
        logging.info("No existing weights, using mode method")
        
        # This holds weight by probe. Weight scales probe value distribution to
        # mode 1.0.
        weights = {}
        
        # Load the first sample to get all the probes that are available
        first_sample = wrangler.load_normalized(samples[0])
        
        for probe in first_sample.iterkeys():
            logging.debug("Weighting probe {}".format(probe))
            
            # For each probe, make a list of its values in all the samples.
            probe_values = []
            
            for sample in samples:
                probe_values.append(wrangler.get_normalized(sample, probe))
                
                
            # Estimate a nonparametric PDF for the distribution of that list.
            pdf_estimate = scipy.stats.gaussian_kde(numpy.asarray(probe_values))
            
            # Find the global maximum of that PDF. This requires minimizing its
            # negation.
            neg_pdf = lambda intensity: -pdf_estimate(intensity)
            
            # Get the optimization result struct
            result = scipy.optimize.minimize_scalar(neg_pdf, 
                bracket=(min(probe_values), max(probe_values)))
                
            if not result.success:
                # Couldn't do the optimization to find the mode for some reason.
                raise Exception("Optimization failed: {}".format(
                    result.message))
            
            # Use the reciprocal of the intensity there as the probe weight.
            weights[probe] = 1.0 / result.x
            
        
    logging.info("Writing {} weights".format(len(weights)))
        
    # Get the writer to the weights file to write.
    writer = tsv.TsvWriter(open(wrangler.weights_path(options.array), "w"))
        
    # Document the format for curious people
    writer.comment("Probe\tWeight\tContributing Samples")
        
    # Write out the weights to the file. TODO: Compute and write at the same
    # time.
    for probe, weight in weights.iteritems():
        writer.line(probe, weight, contributing_samples[probe])
    

if __name__ == "__main__" :
    sys.exit(main(sys.argv))
