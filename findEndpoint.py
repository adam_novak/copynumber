#!/usr/bin/env python2.7
"""
findEndpoint.py: Find one endpoint of a copy number change.

Re-uses sample code and documentation from 
<http://users.soe.ucsc.edu/~karplus/bme205/f12/Scaffold.html>
"""


import argparse, logging, sys, os, collections, math, random, itertools
import warnings, subprocess

import pulp

import tsv
import pulp
import numpy, scipy.stats

import datawrangler, findCopyNumber


def parse_args(args):
    """
    Takes in the command-line arguments list (args), and returns a nice argparse
    result with fields for all the options.
    
    Borrows heavily from the argparse documentation examples:
    <http://docs.python.org/library/argparse.html>
    """
    
    # Construct the parser (which is stored in parser)
    # Module docstring lives in __doc__
    # See http://python-forum.com/pythonforum/viewtopic.php?f=3&t=36847
    # And a formatter class so our examples in the docstring look good. Isn't it
    # convenient how we already wrapped it to 80 characters?
    # See http://docs.python.org/library/argparse.html#formatter-class
    parser = argparse.ArgumentParser(description=__doc__, 
        formatter_class=argparse.RawDescriptionHelpFormatter)
    
    # Logging options
    parser.add_argument("--loglevel", default="DEBUG", choices=["DEBUG", "INFO",
        "WARNING", "ERROR", "CRITICAL"],
        help="logging level to use")
        
    # General options
    parser.add_argument("database_root",
        help="location of sample database directory tree")
    parser.add_argument("sample",
        help="ID of sample to process from the database")
    parser.add_argument("genome",
        help="2bit genome file to use")
    parser.add_argument("contig",
        help="contig to process")
    parser.add_argument("--start", type=int, default=None,
        help="fixed starting position of the copy number difference")
    parser.add_argument("--end", type=int, default=None,
        help="fixed ending position of the copy number difference")
    parser.add_argument("--possible_start", type=int, default=None,
        help="first possible location of the other end")
    parser.add_argument("--possible_end", type=int, default=None,
        help="last possible location of the other end")
    parser.add_argument("--score_weight", type=float, default=1.0,
        help="penalty weight for breakpoint alignment-score-based penalties")
    parser.add_argument("--score_threshold", type=float, default=0,
        help="minimum score to allow a copy number change at a breakpoint")
    parser.add_argument("--save_scores", type=argparse.FileType("w"), 
        default=sys.stdout,
        help="file to save a BEDgraph of breakpoint scores to")
    parser.add_argument("--score_suffix", default="",
        help="suffix to add to score track name")
    parser.add_argument("--save_sample", type=argparse.FileType("w"), 
        default=sys.stdout,
        help="file to save a BEDgraph of inferred sample copy number to")
    parser.add_argument("--sample_suffix", default="",
        help="suffix to add to sample track name")
    parser.add_argument("--mismatch_cost", type=int, default=100,
        help="mismatch penalty for scoring recombination sites")
        
    # LP tuning options
    parser.add_argument("--probe_penalty", type=float, default=10,
        help="approximate equality penalty term for sample/normal ratio")
    parser.add_argument("--sample_dna_penalty", type=float, default=1E-6,
        help="penalty to charge per base of added/lost sample DNA")
    parser.add_argument("--normal_dna_penalty", type=float, default=1,
        help="penalty to charge per base of added/lost normal DNA")
    parser.add_argument("--genome_length", type=float, default=3E12,
        help="length of the genome in base pairs")
    

    # The command line arguments start with the program name, which we don't
    # want to treat as an argument for argparse. So we remove it.
    args = args[1:]
        
    return parser.parse_args(args)

def set_loglevel(loglevel):
    """
    Given a string log level name, set the logging module's log level to that
    level. Raise an exception if the log level is invalid.
    
    """
    
    # Borrows heavily from (actually is a straight copy of) the recipe at
    # <http://docs.python.org/2/howto/logging.html>
    
    # What's the log level number from the module (or None if not found)?
    numeric_level = getattr(logging, loglevel.upper(), None)
    
    if not isinstance(numeric_level, int):
        # Turns out not to be a valid log level
        raise ValueError("Invalid log level: {}".format(loglevel))
    
    # Set the log level to the correct numeric value
    logging.basicConfig(format="%(levelname)s:%(message)s", level=numeric_level)

def get_top_score(genome_file, contig, reference_start, reference_end, 
    query_start, query_end, mismatch_cost=100):
    """
    Use lastz to align the given query range against the given reference range,
    when both are on the given contig of the given genome. Ranges are specified
    as start and one-past-the-end.
    
    mismatch_cost is an optional mismatch penalty (integer) to pass along to
    lastz. The match score is 100.
    
    Returns the score of the best alignment, or 0 if no alignment is found.
    
    """
    
    # What should the reference file specifier be. Convert the end to inclusive
    # coordinates.
    reference = "{}/{}[{},{}]".format(genome_file, contig, reference_start, 
        reference_end-1)
        
    # Do the same for the query
    query = "{}/{}[{},{}]".format(genome_file, contig, query_start, query_end-1)

    # Call lastz. Tell it matches are +100 and mismatches are -100
    lastz = subprocess.Popen(["lastz", "--format=cigar",
        "--match=100,{}".format(mismatch_cost), reference, query], 
        stdout=subprocess.PIPE)
        
    # This holds the best score found so far
    best = 0
        
    for line in lastz.stdout:
        # What score did this alignment get?
        score = int(line.split()[9])
        
        if score > best:
            # New top score
            best = score
            
    logging.debug("Best score aligning {} and {} is {}".format(reference, query,
        best))
            
    # Aligner is all done. Return the best score.
    return best

def main(args):
    """
    Parses command line arguments and do the work of the program.
    "args" specifies the program arguments, with args[0] being the executable
    name. The return value should be used as the program's exit code.
    """
    
    options = parse_args(args) # This holds the nicely-parsed options object

    # Set the log level
    set_loglevel(options.loglevel)
    
    # Make sure exactly one of start and end is specified, and both
    # possible_start and possible_end are specified.
    
    if not (options.start is None) != (options.end is None):
        # They didn't specify the right number of fixed endpoints (one)
        raise Exception("Specify exactly one of --start or --end")
    
    if options.possible_start is None:
        # They didn't give a starting point for where the other end of the copy
        # number difference might fall.
        raise Exception("Specify a --possible_start for the other end")
    
    if options.possible_end is None:
        # They forgot the ending point for where the other end of the copy
        # number difference might fall.
        raise Exception("Specify a --possible_end for the other end")
        
    # This holds the fixed endpoint
    fixed_endpoint = options.start if options.end is None else options.end
    
    # Assume we have a CGH sample.
    
    # Grab a pair of Models for it
    
    # Make a DataWrangler from which to obtain the sample
    wrangler = datawrangler.DataWrangler(options.database_root)
    
    if wrangler.get_technology(options.sample) != "cgh":
        # This sample is the wrong technology to do CGH processing on.
        raise Exception("Sample {} is not CGH".format(options.sample))
    
    # What array does this sample belong to?
    array = wrangler.get_array(options.sample)
    
    # What condition is this sample?
    condition = wrangler.get_condition(options.sample)
    
    # TODO: Put all this loading and weighting stuff in a function.
    
    # This will hold the measured probe hybridization ratios (as floats) by
    # probe name. We read them from the stream that the DataWrangler gives us.
    measured_ratios = findCopyNumber.load_cgh_sample(wrangler.open_sample(
        options.sample), unlog=True,
        log_base=wrangler.get_base(options.sample))
        
    # This holds the path to the corrective per-probe weights for the array
    weights_path = wrangler.weights_path(array)
    
    if os.path.exists(weights_path):
        # The weights file exists. Use it.
        for probe, weight, contributing_samples in tsv.TsvReader(open(
            weights_path)):
            
            if not measured_ratios.has_key(probe):
                # Probe wasn't measured for this sample. Skip it
                continue
            
            # Parse the weight
            weight = float(weight)
            
            # We know the weight to apply to this probe. Apply it in place.
            measured_ratios[probe] *= weight
            logging.debug("Weighted probe {} by {}".format(probe, weight))

    # These hold the Model for the sample genome, the Model for the normal
    # genome, and the SequenceGraphLpProblem that the Models are attached to.
    # Put a 0 breakpoint weight since we plan to do our own breakpoint
    # constraints. TODO: This is a hack. Fix it.
    sample_model, normal_model, problem = findCopyNumber.make_cgh_model(
        wrangler.open_affinities(array), measured_ratios, 
        genome_length=options.genome_length, 
        sample_dna_penalty=options.sample_dna_penalty, 
        normal_dna_penalty=options.normal_dna_penalty, 
        probe_penalty=options.probe_penalty,
        breakpoint_weight=0)   
    
    # Pull out the breakpoint where the fixed end is.
    
    # This holds the previous allele group, since we're going through
    # breakpoints.
    last_allele_group = None
    for allele_group in sample_model.allele_groups[options.contig]:
        if last_allele_group is not None:
        
            if (last_allele_group.end < fixed_endpoint and 
                allele_group.start > fixed_endpoint):
                    
                # This breakpoint is the one at which the fixed endpoint is.
                
                # Save the breakpoint start location
                fixed_start = last_allele_group.end
                # And the breakpoint end location
                fixed_end = allele_group.start
                
        # Advance to the next breakpoint
        last_allele_group = allele_group
    
    # Make a BEDgraph header for the endpoint score BEDgraph we write as we go
    options.save_scores.write("track type=bedGraph name={}-ends{} "
        "description={}-fixed-end-alignment-score alwaysZero=on "
        "yLineMark={} yLineOnOff=on\n".format(
        options.sample, options.score_suffix, options.sample,
        options.score_threshold))
    
    # Add a total other-end copy number changes variable, which must be 1.
    other_ends = pulp.LpVariable("other_end_{}".format(findCopyNumber.get_id()))
    problem.add_constraint(other_ends == 1)
    
    # Keep a list of all the CN change indicator variables that need to
    # contribute to other_ends
    cn_changes = []
        
    # Constrain everywhere outside of (possible_start, possible_end) to have
    # equal copy number, except for the breakpoints specified by start or end.
    
    for contig, allele_groups in sample_model.allele_groups.iteritems():
        # This holds the previous allele group, since we're going through
        # breakpoints.
        last_allele_group = None
        for allele_group in allele_groups:
            if last_allele_group is not None:
                # We should constrain this allele group to the previous one in
                # some way.
                
                if allele_group.contig == options.contig:
                    # Only on this contig can we possibly get out of just fixing
                    # adjacent copy numbers to each other.
                    if (last_allele_group.end < fixed_endpoint and 
                        allele_group.start > fixed_endpoint):
                        
                        # The fixed endpoint is in this breakpoint. No
                        # constraint here. Skip to the next breakpoint.
                        pass
                        
                    
                    elif (last_allele_group.end > options.possible_start and 
                        allele_group.end < options.possible_end):
                            
                        # This breakpoint is in the possible region. Constrain
                        # approximately equal according to the best homology
                        # found in the breakpoint.
                        
                        # Compute the top score aligning the fixed endpoint
                        # against it
                        top_score = get_top_score(options.genome, contig,
                            fixed_start, fixed_end, last_allele_group.end, 
                            allele_group.start,
                            mismatch_cost=options.mismatch_cost)
                        
                        # Calculate the relavent penalty
                        if (top_score == 0 or 
                            top_score < options.score_threshold):
                            
                            # Say we can't even have it here. Too small.
                            problem.add_constraint(allele_group.ploidy == 
                                last_allele_group.ploidy)
                        else:
                            # Say it costs inversely proportional to the score
                            problem.constrain_approximately_equal(
                                last_allele_group.ploidy, allele_group.ploidy, 
                                penalty=options.score_weight/top_score)
                                
                            # Make an indicator variable for a CN change here
                            cn_change = pulp.LpVariable("change_{}".format(
                                findCopyNumber.get_id()))
                            
                            # It's no smaller than the max signed ploidy change.
                            problem.add_constraint(cn_change >= 
                                last_allele_group.ploidy - allele_group.ploidy)
                            problem.add_constraint(cn_change >=
                                allele_group.ploidy - last_allele_group.ploidy)
                                
                            # Penalize it slightly to keep it as small as
                            # possible.
                            problem.add_penalty(1E-6 * cn_change)
                            
                            # Count it as an other end
                            cn_changes.append(cn_change)
                            
                            
                            logging.debug("Penalty: {}".format(
                                options.score_weight/top_score))
                            
                            # Add a BEDgraph entry for this breakpoint
                            options.save_scores.write("{}\t{}\t{}\t{}\n".format(
                                contig, last_allele_group.end, 
                                allele_group.start, top_score))
                            
                    else:
                        # No way to get out of it. Just force the two ploidies
                        # equal.
                        
                        problem.add_constraint(allele_group.ploidy == 
                            last_allele_group.ploidy)
                else:
                    # No way to get out of it. Just force the two ploidies
                    # equal.
                    
                    problem.add_constraint(allele_group.ploidy == 
                        last_allele_group.ploidy)
            
            # Advance to the next breakpoint    
            last_allele_group = allele_group
    
    # Sum up all the cn_changes and make other_ends match it
    problem.add_constraint(other_ends == sum(cn_changes))
                    
    # Solve the model
    problem.solve()
    
    # Verify we have only two copy number changes on our chromosome.
    logging.info("Other ends: {}".format(pulp.value(other_ends)))
    logging.info(str(map(pulp.value, cn_changes)))
    
    # Save sample bedgraph
    sample_model.save_bedgraph(options.save_sample, 
        "{}-r{}".format(options.sample, options.sample_suffix), 
        "{}-restricted{}".format(options.sample, options.sample_suffix))


if __name__ == "__main__" :
    sys.exit(main(sys.argv))
