copynumber: Integer Linear Programming-based Inference of Copy Number
=====================================================================

This repository contains tools designed for taking microarray data from a person and infering how many copies of what regions of the reference genome are present in that person. Unlike other tools for determining copy number from microarray data, this tool is designed to effectively process arrays with probes that map to multiple locations in the genome, allowing even ambiguously-mapped or multi-mapped probes to contribute information towards the copy number calls. Additionally, only integral copy number calls are produced.

The copy number caller considers each mapping location for each probe, and produces an integer copy number determination for each mapping location individually. Internally, the caller converts the problem of deciding on the copy number for each probe mapping location into an integer linear programming (ILP) problem. Desireable properties of the final copy number calls (for example, that they accord reasonably well with the microarray probe measurements, and that adjacent probe mapping locations ought to have equal copy number in the absence of evidence to the contrary) are expressed as constraints on the linear programming (LP) problem, and the LP problem is optimized, yielding a solution which is translated back into a copy number configuration.

Right now, the pipeline has support for Agilent Comparative Genomic Hybridization (CGH) microarrays (or other CGH microarrays that produce a log sample/normal ratio). Support for the copy number variation (CNV) probes on Affymetrix microarrays (like the Human Genome Wide SNP 6.0 chip), and other microarrays that produce raw intensity values for each probe, is a work in progress.

Dependencies
------------

This system depends on the following other software:

- Python 2.7
    - The "tsv" Python module
    - The "rpy2" Python module
    - The "pulp" Python module
    - The "numpy" Python module
    - The "scipy" Python module
    - The "matplotlib" Python module
    - BioPython >=1.61 (with the new SearchIO component)
    
    If you have pip working, all of these modules can be installed by running:
    
        pip install tsv numpy scipy matplotlib rpy2 pulp BioPython
    
- The [CBC Integer Linear Programming solver](https://projects.coin-or.org/Cbc), with its cbc execuatble in your $PATH (although GLPK can be used, CBC is much faster)
- The [lastz DNA aligner](http://www.bx.psu.edu/~rsharris/lastz/), with its lastz executable in your $PATH

For experimental Affymetrix array support, you need:

- The R programming language
    - The "affy" BioConductor package for R
    - The "aroma" BioConductor package for R

How to run the Comparative Genomic Hybridization pipeline
---------------------------------------------------------
- Make a folder to put your data in, which will be referred to as `<root>`. The pipeline uses a "DataWrangler" object which is responsible for going and getting various raw and processed data files from the filesystem, or preparing them if they do not yet exist. It expects to find all of the array and sample files in a single directory with a certain folder structure. Additionally, it expects a file `index.tsv` at the root of that directory, which lists all of the available samples in a format described below.

- Prepare the metadata files for your array.
    - Put FASTA in `<root>/arrays/<array>.fa`
    - Align FASTA to genome, putting PSLX alignment in `<root>/alignments/<array>.pslx`
        - I recommend [pblat](http://code.google.com/p/pblat/), a multi-threaded blat port:
        
                pblat <genome>.2bit <root>/arrays/<array>.fa -minMatch=1 -threads=10 -out=pslx <root>/alignments/<array>.pslx
        
    - Calculate affinities for the array for your region of interest, and store in `<root>/affinities/<array>.tsv`
        
            ./calculateAffinities.py <root>/arrays/<array>.fa <root>/alignments/<array>.pslx --bed <region>.bed --out_file <root>/affinities/<array>.tsv
        
        - This file actually needs to be in that particular place since findCopyNumber.py reads it.
        - You need to specify a region of interest as a BED file; the LP can't scale to the whole genome yet.
            - All probes with any mapping locations in that region will be considered.

- Prepare the data files for your samples.
    - For Agilent scans, put them in `<root>/scans/<array>/<sample>.txt`
    - For non-Agilent arrays, or for samples for which you have misplaced, deleted, accidentally processed, translated through Microsoft Excel and back, or otherwise lost the original scan file, you can use a different format.
        - For each sample, make a simple tab-separated value (TSV) files containing two columns: the probe name and the log sample/normal ratio. Put these files in `<root>/samples/<array>/log10/<sample>.tsv` if you used a base-10 logarithm, or `<root>/samples/<array>/log2/<sample>.tsv` if you used a base-2 logarithm.
        - Note that when scan files are first used, they will be extracted and stored in this intermediate format as well. (Scan files are assumed to use log base 10.)

- Populate the index of all samples, `<root>/index.tsv`.
    - This file has one line for every sample, and four tab separated columns: the sample name, the array name, the technology used (which is "cgh" for CGH samples), and the type of the sample ("Control", or whatever non-control experimental group the sample belongs to).
    - In other words, the lines are of the form `<sample>\t<array>\tcgh\t<type>`, where `\t` represents a tab character.

- Calculate probe-specific weights, in order to normalize by the control samples.

        ./findCopyNumber.py cgh-weights ~/grotto --loglevel INFO --probe_penalty 1 --breakpoint_weight 1 --sample_dna_penalty 0 --normal_dna_penalty 1 ${ARRAY};

    - Writes `<root>/weights/<array>.tsv` based on your current set of controls

- Run the program to infer copy number for all samples that use the array
    
        ./findCopyNumber.py cgh-all <root> <array> --out_dir <output_directory> --probe_penalty 10 --loglevel INFO
        
    - Output will be written as bedgraph files.
