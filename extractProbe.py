#!/usr/bin/env python2.7
"""
extractProbe.py: get the total that a set of probes has in all samples for an
array.

This script takes a database location, array name and probe name(s). It opens
each sample in the array, gets the total value for the probes, and writes a file
with one total per line, suitable for plotting as a histogram.

Extracts and sums weighted sample/normal ratios.

Re-uses sample code and documentation from 
<http://users.soe.ucsc.edu/~karplus/bme205/f12/Scaffold.html>
"""

import argparse, sys, os, collections, math, itertools, logging

import tsv
import datawrangler

def parse_args(args):
    """
    Takes in the command-line arguments list (args), and returns a nice argparse
    result with fields for all the options.
    
    Borrows heavily from the argparse documentation examples:
    <http://docs.python.org/library/argparse.html>
    """
    
    # Construct the parser (which is stored in parser)
    # Module docstring lives in __doc__
    # See http://python-forum.com/pythonforum/viewtopic.php?f=3&t=36847
    # And a formatter class so our examples in the docstring look good. Isn't it
    # convenient how we already wrapped it to 80 characters?
    # See http://docs.python.org/library/argparse.html#formatter-class
    parser = argparse.ArgumentParser(description=__doc__, 
        formatter_class=argparse.RawDescriptionHelpFormatter)
    
    # General options
    parser.add_argument("database_root",
        help="location of sample database directory tree")
    parser.add_argument("array",
        help="name of the array to process")
    parser.add_argument("probes", nargs="+",
        help="name of the probe to extract")
    parser.add_argument("--out_file", type=argparse.FileType("w"),
        default=sys.stdout,
        help="output file of probe sample/normal ratios")
    parser.add_argument("--dump_file", type=argparse.FileType("w"),
        default=None,
        help="File to store probes in order, as a TSV")
        
    # Logging options
    parser.add_argument("--loglevel", default="DEBUG", choices=["DEBUG", "INFO",
        "WARNING", "ERROR", "CRITICAL"],
        help="logging level to use")
        
    # The command line arguments start with the program name, which we don't
    # want to treat as an argument for argparse. So we remove it.
    args = args[1:]
        
    return parser.parse_args(args)
    
def set_loglevel(loglevel):
    """
    Given a string log level name, set the logging module's log level to that
    level. Raise an exception if the log level is invalid.
    
    """
    
    # Borrows heavily from (actually is a straight copy of) the recipe at
    # <http://docs.python.org/2/howto/logging.html>
    
    # What's the log level number from the module (or None if not found)?
    numeric_level = getattr(logging, loglevel.upper(), None)
    
    if not isinstance(numeric_level, int):
        # Turns out not to be a valid log level
        raise ValueError("Invalid log level: {}".format(loglevel))
    
    # Set the log level to the correct numeric value
    logging.basicConfig(format="%(levelname)s:%(message)s", level=numeric_level)

def main(args):
    """
    Parses command line arguments and do the work of the program.
    "args" specifies the program arguments, with args[0] being the executable
    name. The return value should be used as the program's exit code.
    """
    
    options = parse_args(args) # This holds the nicely-parsed options object

    # Set up logging
    set_loglevel(options.loglevel)
    
    # Make a DataWrangler from which to obtain the samples
    wrangler = datawrangler.DataWrangler(options.database_root)
    
    if options.dump_file is not None:
        # Make a TSV writer to write individual probe values by sample
        dump_writer = tsv.TsvWriter(options.dump_file)
    
    for i, sample in enumerate(wrangler.get_samples(options.array)):
        # Load all the ratios for this sample, with weights and normalization if
        # applicable.
        ratios = wrangler.load_weighted_ratios(sample)
        
        # This holds the total normalized ratio for all the probes. It's kind of
        # hard to interpret, but one would expect it to be bimodal if the group
        # of probes summed vary together.
        total = 0
        for probe in options.probes:
            total += ratios[probe]
        
        # Print out the total of the probes we care about
        options.out_file.write("{}\n".format(total))
        
        if options.dump_file is not None:
            # Save the individual probe values
            dump_writer.list_line([sample] + [ratios[probe] for probe in 
                options.probes])
        
        logging.info("Done with sample #{}: {}".format(i, sample))

if __name__ == "__main__" :
    sys.exit(main(sys.argv))
