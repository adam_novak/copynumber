#!/usr/bin/env python2.7
"""
microarray.py: module for parsing (Agilent) microarray data files.

Loosely based on agilent_parser.py by Adam Ewing.

Can also be used as a script to read a microarray data file and extract the
values of specific fields for each probe record.

"""

import collections, sys, argparse
import tsv

class MicroarrayRecord(dict):
    """
    A class to represent a microarray data file record. Like a dict, but you can
    attach extra attributes to it, since it's a real class.
    """
    pass

def parse_section(array_file, section_name):
    """
    Given an input stream, parse an Alilent-format section until the terminating
    "*" line or the end of the file, whichever comes first. Yield a
    MicroarrayRecord (a dict derivative with each field in it by name) for each
    data line, with strings parsed out as the appropriate type. Looks for field
    names on a line starting with the given section_name; if they are not found,
    an error is raised.
    
    """
    
    # This holds a list of the type for each column. Valid types are: text,
    # float, integer, and boolean.
    types = None
    # This holds a list of the field names for each column
    names = None
    
    for parts in tsv.TsvReader(array_file):
        if parts[0] == "*":
            # Section is over
            break
        elif parts[0] == "TYPE":
            # This is a list of types. Keep it around.
            types = parts[1:]
        elif parts[0] == section_name:
            # This is the field names for this section. Keep them around.
            names = parts[1:]
        elif parts[0] == "DATA":
            # This is a data line. Parse it and yield it.
            if types is None:
                # We don't know what types things should be yet.
                raise Exception("DATA line before TYPE line")
            if names is None:
                # We don't have the column names yet.
                raise Exception("DATA line before {} line".format(section_name))
                
            # This holds the dict of column values by column name that we will
            # return.
            record = MicroarrayRecord()
                
            for i, string_data in enumerate(parts[1:]):
                # This holds the correctly-typed parsed value for this column
                value = None
                
                if types[i] == "text":
                    # It's a string. No parsing needed.
                    value = string_data
                elif types[i] == "float":
                    # Parse it as a float
                    try:
                        value = float(string_data)
                    except:
                        # File may have been corrupted by biologists who ran ti
                        # through Excel and turned numbers into dates. Hopefully
                        # we don't need this column. Just put NaN if we can't
                        # read it.
                        value = float("NaN")
                elif types[i] == "integer":
                    # Parse it as an integer
                    try:
                        value = int(string_data)
                    except:
                        # Just put NaN if we can't read it.
                        value = float("NaN")
                elif types[i] == "boolean":
                    # Parse it as a boolean, which is somewhat tricky because
                    # booleans are stored as 0/1 and Python's bool() interprets
                    # "0" as True.
                    value = (string_data == "1")
                else:
                    # Never heard of this type.
                    raise Exception("Invalid type: {}".format(types[i]))
                
                # Put the parsed column value in the record
                record[names[i]] = value
            
            # Send the record out for processing
            yield record
        
        else:
            # This isn't the right line type here
            raise Exception("Invalid line type {} in {} section".format(
                parts[0], section_name))
                
                    
    

def parse(array_file):
    """
    Given an input stream with Agilent-format microarray data, yield a record
    for each probe (i.e. each DATA line in the FEATURES section).
    
    Each record is a MicroarrayRecord (a dict derivative) from FEATURES section
    column name to correctly- typed value, with a .stats attribute set to a
    MicroarrayRecord of the STATS section and a .feparams attribute set to a
    MicroarrayRecord of the FEPARAMS section.
    
    Be aware that some "probes" are really control probes and not the things you
    are probably interested in. The "ControlType" field (if your data has it)
    can tell you whether a probe is a control or not.
    
    """
    
    # Get the one record from the first section (FEPARAMS)
    feparams = list(parse_section(array_file, "FEPARAMS"))[0]
    
    # Get the one record from the second section (STATS)
    stats = list(parse_section(array_file, "STATS"))[0]
    
    for probe_record in parse_section(array_file, "FEATURES"):
        # Prepare and yield each probe record
        
        # Attach the array metadata to each probe (by reference!), in case
        # someone wants to look at it.
        setattr(probe_record, "feparams", feparams)
        setattr(probe_record, "stats", stats)
        
        # Yield the final probe record so the caller can use it
        yield probe_record

####
# We can also use this file as a script to extract a field from a microarray
# results file.
####

def _parse_args(args):
    """
    Takes in the command-line arguments list (args), and returns a nice argparse
    result with fields for all the options.
    
    Borrows heavily from the argparse documentation examples:
    <http://docs.python.org/library/argparse.html>
    """
    
    # Construct the parser (which is stored in parser)
    # Module docstring lives in __doc__
    # See http://python-forum.com/pythonforum/viewtopic.php?f=3&t=36847
    # And a formatter class so our examples in the docstring look good. Isn't it
    # convenient how we already wrapped it to 80 characters?
    # See http://docs.python.org/library/argparse.html#formatter-class
    parser = argparse.ArgumentParser(description=__doc__, 
        formatter_class=argparse.RawDescriptionHelpFormatter)
    
    # General options
    parser.add_argument("microarray_file", type=argparse.FileType("r"),
        help="Agilent-format microarray data file")
    parser.add_argument("field_names", nargs="+", type=str,
        help="names of fields to extract from probe records")
        
    # The command line arguments start with the program name, which we don't
    # want to treat as an argument for argparse. So we remove it.
    args = args[1:]
        
    return parser.parse_args(args)

def _main(args):
    """
    Parses command line arguments and do the work of the program.
    "args" specifies the program arguments, with args[0] being the executable
    name. The return value should be used as the program's exit code.
    """
    
    options = _parse_args(args) # This holds the nicely-parsed options object

    # This holds a TSV writer for the output TSV
    writer = tsv.TsvWriter(sys.stdout)

    for probe_record in parse(options.microarray_file):
        # Print the requested fields from every probe record.
        writer.list_line([probe_record[field] for field in options.field_names])
    
if __name__ == "__main__" :
    sys.exit(_main(sys.argv))
        
    
    
    
    
    

