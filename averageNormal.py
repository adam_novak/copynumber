#!/usr/bin/env python2.7
"""
averageNormal.py: Average the normal signals for each probe over microarrays.

Produces output of the form <probe name>\t<avergae>\t<standard deviation>

Re-uses sample code and documentation from 
<http://users.soe.ucsc.edu/~karplus/bme205/f12/Scaffold.html>
"""

import argparse, sys, os, collections, math, itertools

import microarray

def parse_args(args):
    """
    Takes in the command-line arguments list (args), and returns a nice argparse
    result with fields for all the options.
    
    Borrows heavily from the argparse documentation examples:
    <http://docs.python.org/library/argparse.html>
    """
    
    # Construct the parser (which is stored in parser)
    # Module docstring lives in __doc__
    # See http://python-forum.com/pythonforum/viewtopic.php?f=3&t=36847
    # And a formatter class so our examples in the docstring look good. Isn't it
    # convenient how we already wrapped it to 80 characters?
    # See http://docs.python.org/library/argparse.html#formatter-class
    parser = argparse.ArgumentParser(description=__doc__, 
        formatter_class=argparse.RawDescriptionHelpFormatter)
    
    # General options
    parser.add_argument("array_files", nargs="+", type=argparse.FileType("r"),
        help="Agilent-format microarray files")
    parser.add_argument("--field_name", type=str, default="gProcessedSignal",
        help="name of field to average")
        
    # The command line arguments start with the program name, which we don't
    # want to treat as an argument for argparse. So we remove it.
    args = args[1:]
        
    return parser.parse_args(args)

def main(args):
    """
    Parses command line arguments and do the work of the program.
    "args" specifies the program arguments, with args[0] being the executable
    name. The return value should be used as the program's exit code.
    """
    
    options = parse_args(args) # This holds the nicely-parsed options object

    # This holds running totals by probe name
    totals = collections.defaultdict(float)
    
    # This holds running second moment totals (sum of squares of data items) by
    # probe name
    square_totals = collections.defaultdict(float)
    
    for array_file in options.array_files:
        for probe_record in microarray.parse(array_file):
            # Add the field's value to the running total for the appropriate
            # probe.
            totals[probe_record["ProbeName"]] += probe_record[
                options.field_name]
                
            # And add the square of it to the square total
            square_totals[probe_record["ProbeName"]] += probe_record[
                options.field_name] ** 2
                
    for probe_name, total in totals.iteritems():
        # Calculate the average
        average = total / len(options.array_files)
        
        # Calculate the second moment
        second_moment = square_totals[probe_name] / len(options.array_files)
    
        # Calculate the variance
        variance = second_moment - average ** 2
    
        if variance > 0:
            # Calculate the standard deviation
            standard_deviation = math.sqrt(variance)
        else:
            # We can't have a standard deviation
            standard_deviation = float("NaN")
    
        # Print the average and standard deviation for each probe
        print "{}\t{}\t{}".format(probe_name, average, standard_deviation)   
    
    

if __name__ == "__main__" :
    sys.exit(main(sys.argv))
