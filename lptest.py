#!/usr/bin/env python2.7
# lptest.py: Test linear programming.

import pulp

problem = pulp.LpProblem("awesomeproblem", pulp.LpMaximize)

# We can have 0 or more cows
cows = pulp.LpVariable("cows", 0, cat="Integer")

# We can have 0 to 10 chickens
chickens = pulp.LpVariable("chickens", 0, 10, cat="Integer")

# They have a money cost
cost = pulp.LpVariable("cost", 0)

# Add the objective
problem += 5 * cows + 2 * chickens

# Add some constraints
problem += cows <= chickens
problem += 10 * cows + 2 * chickens == cost
problem += cost <= 100

# Solve the problem (quietly)
status = problem.solve(pulp.GLPK(msg=0))
print pulp.LpStatus[status]

for var in [cows, chickens, cost]:
    print var
    print pulp.value(var)
